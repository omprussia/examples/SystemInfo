// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusReply>
#include <QtDBus/QtDBus>

#include "cdbconstants.h"
#include "cdbtechnology.h"

CDBTechnology::CDBTechnology(QObject *parent) : QObject(parent) { }

CDBTechnology::CDBTechnology(const QString &name, QObject *parent) : QObject(parent), m_name(name)
{
    QDBusConnection systemBus = QDBusConnection::systemBus();
    systemBus.connect(CDBConstants::s_service, m_name, CDBConstants::s_technologyInterface,
                      CDBConstants::s_signalPropertyChanged, this,
                      SLOT(onPropertyChanged(const QString &, const QDBusVariant &)));

    QDBusInterface interface(CDBConstants::s_service, m_name, CDBConstants::s_technologyInterface,
                             QDBusConnection::systemBus());
    QDBusReply<QVariantMap> propertiesReply = interface.call(CDBConstants::s_methodGetProperties);

    if (propertiesReply.isValid())
        m_properties = propertiesReply.value();
}

QString CDBTechnology::name() const
{
    return m_name.split(QChar('/')).last();
}

QVariant CDBTechnology::getProperty(const QString &name) const
{
    return m_properties.value(name);
}

void CDBTechnology::onPropertyChanged(const QString &name, const QDBusVariant &value)
{
    m_properties.insert(name, value.variant());
    emit propertyChanged(name);
}
