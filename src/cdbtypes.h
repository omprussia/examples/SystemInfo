// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CDBTYPES_H
#define CDBTYPES_H

#include <QPair>
#include <QtDBus/QDBusObjectPath>

typedef QPair<QDBusObjectPath, QVariantMap> Service;
typedef QList<Service> ServiceList;
typedef QList<QDBusObjectPath> DBusObjectList;
Q_DECLARE_METATYPE(Service)
Q_DECLARE_METATYPE(ServiceList)
Q_DECLARE_METATYPE(DBusObjectList)

#endif // CDBTYPES_H
