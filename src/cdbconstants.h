// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CDBCONSTANTS_H
#define CDBCONSTANTS_H

#include <QString>

namespace CDBConstants {
static const QString s_service = QStringLiteral("net.connman");
static const QString s_managerInterface = QStringLiteral("net.connman.Manager");
static const QString s_serviceInterface = QStringLiteral("net.connman.Service");
static const QString s_technologyInterface = QStringLiteral("net.connman.Technology");
static const QString s_path = QStringLiteral("/");
static const QString s_techWiFiPath = QStringLiteral("/net/connman/technology/wifi");
static const QString s_techBluetoothPath = QStringLiteral("/net/connman/technology/bluetooth");
static const QString s_techGPSPath = QStringLiteral("/net/connman/technology/gps");
static const QString s_techPath = QStringLiteral("/net/connman/technology/");

static const QString s_signalPropertyChanged = QStringLiteral("PropertyChanged");
static const QString s_signalServicesChanged = QStringLiteral("ServicesChanged");
static const QString s_signalTechnologyAdded = QStringLiteral("TechnologyAdded");
static const QString s_signalTechnologyRemoved = QStringLiteral("TechnologyRemoved");
static const QString s_methodGetProperties = QStringLiteral("GetProperties");
static const QString s_methodGetServices = QStringLiteral("GetServices");
static const QString s_methodGetTechnologies = QStringLiteral("GetTechnologies");

static const int s_wifiUpdateIntervalMsec = 3000;
} // namespace CDBConstants

#endif // CDBCONSTANTS_H
