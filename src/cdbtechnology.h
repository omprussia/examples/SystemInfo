// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef CDBTECHNOLOGY_H
#define CDBTECHNOLOGY_H

#include <QObject>
#include <QVariantMap>
#include <QtDBus/QDBusVariant>

class CDBTechnology : public QObject
{
    Q_OBJECT

public:
    explicit CDBTechnology(QObject *parent = nullptr);
    CDBTechnology(const QString &name, QObject *parent = nullptr);

    QString name() const;
    QVariant getProperty(const QString &name) const;

signals:
    void propertyChanged(QString);

private slots:
    void onPropertyChanged(const QString &name, const QDBusVariant &value);

private:
    QString m_name;
    QVariantMap m_properties;
};

#endif // CDBTECHNOLOGY_H
