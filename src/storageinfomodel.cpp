// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "storageinfomodel.h"
#include <QSet>
#include <QStandardPaths>

StorageInfoModel::StorageInfoModel(QObject *parent) : QAbstractListModel(parent)
{
    refresh();
}

int StorageInfoModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_items.count();
}

QHash<int, QByteArray> StorageInfoModel::roleNames() const
{
    return {
        { BlockSizeRole, QByteArrayLiteral("blockSize") },
        { BytesAvailableRole, QByteArrayLiteral("bytesAvailable") },
        { BytesFreeRole, QByteArrayLiteral("bytesFree") },
        { BytesTotalRole, QByteArrayLiteral("bytesTotal") },
        { DeviceRole, QByteArrayLiteral("device") },
        { DisplayNameRole, QByteArrayLiteral("displayName") },
        { FileSystemTypeRole, QByteArrayLiteral("fileSystemType") },
        { IsReadOnlyRole, QByteArrayLiteral("isReadOnly") },
        { IsReadyRole, QByteArrayLiteral("isReady") },
        { IsRootRole, QByteArrayLiteral("isRoot") },
        { NameRole, QByteArrayLiteral("name") },
        { RootPathRole, QByteArrayLiteral("rootPath") },
    };
}

QVariant StorageInfoModel::data(const QModelIndex &index, int role) const
{
    auto iItem = index.row();
    if (iItem < 0 || iItem >= m_items.count())
        return QVariant();
    auto item = m_items[iItem];
    switch (role) {
    case BlockSizeRole:
        return item.blockSize();
    case BytesAvailableRole:
        return item.bytesAvailable();
    case BytesFreeRole:
        return item.bytesFree();
    case BytesTotalRole:
        return item.bytesTotal();
    case DeviceRole:
        return item.device();
    case DisplayNameRole:
        return item.displayName();
    case FileSystemTypeRole:
        return item.fileSystemType();
    case IsReadOnlyRole:
        return item.isReadOnly();
    case IsReadyRole:
        return item.isReady();
    case IsRootRole:
        return item.isRoot();
    case NameRole:
        return item.name();
    case RootPathRole:
        return item.rootPath();
    default:
        return QVariant();
    }
}

void StorageInfoModel::refresh()
{
    QSet<QByteArray> addedDevices;

    beginResetModel();
    m_items.clear();
    for (auto &mountedVolume : QStorageInfo::mountedVolumes()) {
        if (!mountedVolume.isValid())
            continue;

        /* Skip all the tmpfs, since they are based on RAM */
        QByteArray device = mountedVolume.device();
        if (device == "tmpfs")
            continue;

        /* Volumes might be mounted in more than one place: make sure we only
         * show one of them */
        if (!addedDevices.contains(device)) {
            m_items.append(mountedVolume);
            addedDevices.insert(device);
        }
    }
    endResetModel();
}
