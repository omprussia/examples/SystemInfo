// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CDBUTILS_H
#define CDBUTILS_H

#include <QString>
#include <QFile>
#include <QTextStream>

#define DECLARE_PROPERTY(Type, name)                   \
  Q_PROPERTY(Type name READ name NOTIFY name##Changed) \
  public:                                              \
  Type name() const;                                   \
  Q_SIGNAL void name##Changed(Type);                   \
                                                       \
  private:

#define CHECK_PROPERTY(DBusName, propName, testName) \
  if (testName == QStringLiteral(DBusName)) {        \
    emit propName##Changed(propName());              \
  }

namespace CDBUtils {

[[maybe_unused]] static QString defaultRouteName()
{
    // NOTE:
    // https://github.com/sailfishos/libconnman-qt/blob/master/libconnman-qt/networkmanager.cpp#L754
    QString defaultNetDev;
    QFile routeFile("/proc/net/route");
    if (routeFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&routeFile);
        QString line = in.readLine();
        while (!line.isNull()) {
            QStringList lineList = line.split('\t');
            if (lineList.size() >= 11) {
                if ((lineList.at(1) == "00000000" && lineList.at(3) == "0003")
                    || (lineList.at(0).startsWith("ppp") && lineList.at(3) == "0001")) {
                    defaultNetDev = lineList.at(0);
                    break;
                }
            }
            line = in.readLine();
        }
        routeFile.close();
    }

    if (defaultNetDev.isNull()) {
        QFile ipv6routeFile("/proc/net/ipv6_route");
        if (ipv6routeFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream ipv6in(&ipv6routeFile);
            QString ipv6line = ipv6in.readLine();
            while (!ipv6line.isNull()) {
                QStringList ipv6lineList = ipv6line.split(QRegExp("\\s+"));
                if (ipv6lineList.size() >= 10) {
                    if (ipv6lineList.at(0) == "00000000000000000000000000000000"
                        && (ipv6lineList.at(8).endsWith("3")
                            || (ipv6lineList.at(8).endsWith("1")))) {
                        defaultNetDev = ipv6lineList.at(9).trimmed();
                        break;
                    }
                    ipv6line = ipv6in.readLine();
                }
            }
            ipv6routeFile.close();
        }
    }

    return defaultNetDev;
}

[[maybe_unused]] static QString techPathToName(const QString &techName)
{
    auto defaultServiceTechNameSplit = techName.split(QStringLiteral("/"));
    if (defaultServiceTechNameSplit.isEmpty())
        return {};

    return defaultServiceTechNameSplit.last().split(QStringLiteral("_")).first();
}
} // namespace CDBUtils

#endif // CDBUTILS_H
