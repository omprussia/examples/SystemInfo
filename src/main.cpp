// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QGuiApplication>
#include <QtQml>
#include <QtSystemInfo/QBatteryInfo>
#include <QtSystemInfo/QDeviceInfo>
#include <QQuickView>

#include <auroraapp.h>

#include "storageinfomodel.h"
#include "featuresiface.h"
#include "iudidiface.h"
#include "simcardsiface.h"
#include "storagesiface.h"

#include "cdbtechnologymodel.h"
#include "cdbservice.h"
#include "cdbnetworkmanager.h"

#include "packageinfoprovider.h"
#include "packagesearcher.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<QBatteryInfo>("ru.omp.SystemInfo", 1, 0, "BatteryInfo");
    qmlRegisterType<QDeviceInfo>("ru.omp.SystemInfo", 1, 0, "DeviceInfo");
    qmlRegisterType<StorageInfoModel>("ru.omp.SystemInfo", 1, 0, "StorageInfoModel");
    qmlRegisterType<FeaturesIface>("ru.omp.SystemInfo", 1, 0, "FeaturesIface");
    qmlRegisterType<IudidIface>("ru.omp.SystemInfo", 1, 0, "IudidIface");
    qmlRegisterType<SimCardsIface>("ru.omp.SystemInfo", 1, 0, "SimCardsIface");
    qmlRegisterType<StoragesIface>("ru.omp.SystemInfo", 1, 0, "StoragesIface");

    qmlRegisterType<CDBTechnologyModel>("ru.omp.SystemInfo", 1, 0, "CDBTechnologyModel");
    qmlRegisterType<CDBService>("ru.omp.SystemInfo", 1, 0, "CDBService");
    qmlRegisterType<CDBNetworkManager>("ru.omp.SystemInfo", 1, 0, "CDBNetworkManager");
    //    qmlRegisterUncreatableType<CDBService::SecurityType>("ru.omp.SystemInfo", 1, 0,
    //                                                         "SecurityType",
    //                                                         "Cannot create a class of type
    //                                                         UncreatableType");

    qmlRegisterType<PackageInfoProvider>("ru.omp.SystemInfo", 1, 0, "PackageInfoProvider");
    qmlRegisterType<PackageSearcher>("ru.omp.SystemInfo", 1, 0, "PackageSearcher");

    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    QScopedPointer<QQuickView> view(Aurora::Application::createView());

    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/SystemInfo.qml")));
    view->show();

    return application->exec();
}
