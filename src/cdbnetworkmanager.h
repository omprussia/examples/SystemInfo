// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CDBNETWORKMANAGER_H
#define CDBNETWORKMANAGER_H

#include <QObject>
#include <QtDBus/QDBusVariant>

#include "cdbutils.h"
#include "cdbtypes.h"
#include "cdbservice.h"

class CDBTechnology;
class CDBNetworkManager : public QObject
{
    Q_OBJECT

    // WARNING: TBD?
    //    DECLARE_PROPERTY(bool, servicesEnabled)
    //    DECLARE_PROPERTY(bool, technologiesEnabled)

    DECLARE_PROPERTY(bool, connected)
    DECLARE_PROPERTY(bool, connecting)
    DECLARE_PROPERTY(bool, available)
    DECLARE_PROPERTY(bool, valid)
    DECLARE_PROPERTY(bool, offlineMode)
    DECLARE_PROPERTY(bool, sessionMode)
    DECLARE_PROPERTY(QString, state)
    DECLARE_PROPERTY(int, inputRequestTimeout)
    DECLARE_PROPERTY(QStringList, technologies)

    DECLARE_PROPERTY(CDBService *, defaultRoute)
    DECLARE_PROPERTY(CDBService *, connectedWifi)

public:
    enum ManagerState {
        Idle,
        Failure,
        Association,
        Configuration,
        Ready,
        Disconnect,
        Online,
        Unknow = 99
    };
    Q_ENUM(ManagerState)

    explicit CDBNetworkManager(QObject *parent = nullptr);

    static QString stateToString(ManagerState state);
    static ManagerState stringToState(const QString &state);

private slots:
    void onPropertyChanged(const QString &name, const QDBusVariant &value);
    void onServicesChanged(const ServiceList &changed, const DBusObjectList &removed);
    void onTechAdded(const QDBusObjectPath &path, const QVariantMap &prop);
    void onTechRemoved(const QDBusObjectPath &path);

private:
    void queryDefaultRoute();
    void queryTechnologies();

private:
    QVariantMap m_properties;
    QScopedPointer<CDBService> m_defaultService;
    QScopedPointer<CDBService> m_connectedWiFi;
    QMap<QString, CDBTechnology *> m_technologies;
};

#endif // CDBNETWORKMANAGER_H
