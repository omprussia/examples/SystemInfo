// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef STORAGEINFOMODEL_H
#define STORAGEINFOMODEL_H

#include <QAbstractListModel>
#include <QtCore/QStorageInfo>

class StorageInfoModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role {
        BlockSizeRole = Qt::UserRole + 1,
        BytesAvailableRole,
        BytesFreeRole,
        BytesTotalRole,
        DeviceRole,
        DisplayNameRole,
        FileSystemTypeRole,
        IsReadOnlyRole,
        IsReadyRole,
        IsRootRole,
        NameRole,
        RootPathRole,
    };

    explicit StorageInfoModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;

public slots:
    void refresh();

private:
    QList<QStorageInfo> m_items;
};

#endif // STORAGEINFOMODEL_H
