// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CDBTECHNOLOGYMODEL_H
#define CDBTECHNOLOGYMODEL_H

#include <QAbstractListModel>
#include <QtDBus/QDBusVariant>

#include "cdbtypes.h"

class QTimer;
class CDBService;
class CDBTechnologyModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(bool powered READ powered NOTIFY poweredChanged)
    Q_PROPERTY(bool available READ available NOTIFY availableChanged)

public:
    enum ManagerRoles {
        ServiceRole = Qt::UserRole + 1,
    };

    explicit CDBTechnologyModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    QString name() const;
    bool powered() const;
    bool available() const;

    Q_INVOKABLE void init();

public slots:
    void setName(QString name);
    void requestScan();

signals:
    void nameChanged(QString name);
    void poweredChanged(bool powered);
    void availableChanged(bool available);

private slots:
    void onTechWiFiChanged(const QString &name, const QDBusVariant &val);
    void onServicesChanged(const ServiceList &, const DBusObjectList &);
    void scanWifi() const;

private:
    QString m_name;

    QMap<QString, CDBService *> m_cachedServices;
    bool m_powered{ false };
    bool m_available{ false };
    QTimer *m_timer;
};

#endif // CDBTECHNOLOGYMODEL_H
