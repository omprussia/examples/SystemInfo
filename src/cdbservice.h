// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef CDBSERVICE_H
#define CDBSERVICE_H

#include <QObject>
#include <QVariantMap>
#include <QtDBus/QDBusVariant>

#include "cdbutils.h"

class CDBService : public QObject
{
    Q_OBJECT

    DECLARE_PROPERTY(QString, name);
    DECLARE_PROPERTY(QString, state)
    DECLARE_PROPERTY(QString, type)
    DECLARE_PROPERTY(uint, strength)
    DECLARE_PROPERTY(bool, autoConnect)
    DECLARE_PROPERTY(bool, valid)
    DECLARE_PROPERTY(bool, favorite)
    DECLARE_PROPERTY(bool, connected)
    DECLARE_PROPERTY(bool, connecting)
    DECLARE_PROPERTY(QStringList, nameservers)
    DECLARE_PROPERTY(QStringList, nameserversConfig)
    DECLARE_PROPERTY(QStringList, timeservers)
    DECLARE_PROPERTY(QStringList, timeserversConfig)
    DECLARE_PROPERTY(QStringList, domains)
    DECLARE_PROPERTY(QStringList, domainsConfig)
    DECLARE_PROPERTY(QVariantMap, ipv4)
    DECLARE_PROPERTY(QVariantMap, ipv4Config)
    DECLARE_PROPERTY(QVariantMap, ipv6)
    DECLARE_PROPERTY(QVariantMap, ipv6Config)
    DECLARE_PROPERTY(QString, bssid)
    DECLARE_PROPERTY(bool, hidden)
    DECLARE_PROPERTY(QStringList, security)
    DECLARE_PROPERTY(int, securityType)
    DECLARE_PROPERTY(QString, privateKey)
    DECLARE_PROPERTY(quint32, maxRate)
    DECLARE_PROPERTY(quint16, frequency)
    DECLARE_PROPERTY(QString, encryptionMode)
    DECLARE_PROPERTY(QString, privateKeyPassphrase)
    DECLARE_PROPERTY(bool, roaming)
    DECLARE_PROPERTY(QString, technicalName)

public:
    enum SecurityType { SecurityUnknown, SecurityNone, SecurityWEP, SecurityPSK, SecurityIEEE802 };
    Q_ENUM(SecurityType)

    explicit CDBService(QObject *parent = nullptr);
    explicit CDBService(const QString &name, QObject *parent = nullptr);

    QVariant getProperty(const QString &name) const;
    bool isDefault() const;
    void forceUpdate();
    QString serviceName() const;

private slots:
    void onPropertyChanged(const QString &name, const QDBusVariant &val);

private:
    void queryProperty();

signals:
    void propertyChanged(QString, QVariant);
    void propertyUpdated();
    void defaultChanged(bool);

private:
    QVariantMap m_properties;
    QString m_name;
    bool m_isDefault;
};

#endif // CDBSERVICE_H
