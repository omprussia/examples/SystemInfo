// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PACKAGEKITPROVIDER_H
#define PACKAGEKITPROVIDER_H

#include <QObject>
#include <QString>
#include <QVariantList>
#include <PackageKitAdaptor>

/*!
 * \brief Provides a list of packages available on the device.
 */
class PackageSearcher : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool daemonRunning READ daemonRunning NOTIFY daemonRunningChanged)
    Q_PROPERTY(QVariantList packageList READ packageList NOTIFY packageListChanged)
    Q_PROPERTY(bool searchFailed READ searchFailed NOTIFY searchFailedChanged)
    Q_PROPERTY(bool searchSucceeded READ searchSucceeded NOTIFY searchSucceededChanged)
    Q_PROPERTY(bool searchRunning READ searchRunning NOTIFY searchRunningChanged)
    Q_PROPERTY(QString searchString READ searchString NOTIFY searchStringChanged)

public:
    explicit PackageSearcher(QObject *parent = nullptr);

    Q_INVOKABLE bool daemonRunning() const;
    Q_INVOKABLE QVariantList packageList() const;
    Q_INVOKABLE bool searchFailed() const;
    Q_INVOKABLE bool searchSucceeded() const;
    Q_INVOKABLE void searchPackageByName(const QString &name);
    Q_INVOKABLE bool searchRunning() const;
    Q_INVOKABLE QString searchString() const;

Q_SIGNALS:
    void daemonRunningChanged();
    void packageListChanged();
    void searchFailedChanged();
    void searchSucceededChanged();
    void searchRunningChanged();
    void searchStringChanged();

private:
    void _storeDaemonRunningState(bool running);
    void _processSearchResults(PackageKitAdaptor::Exit status, uint runtime);
    void _addPackageInfo(PackageKitAdaptor::Info info, const QString &packageID,
                         const QString &summary);

    bool m_daemonRunning;
    QVariantList m_packageList;
    QString m_searchString;
    PackageKitAdaptor::Transaction *m_searchTransaction;
};

#endif // PACKAGEKITPROVIDER_H
