# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

QT += core dbus

INCLUDEPATH += $$PWD/include

DEPENDPATH += $$PWD/include

HEADERS_PUBLIC = \
    $$PWD/include/featuresiface.h \
    $$PWD/include/iudidiface.h \
    $$PWD/include/storagesiface.h \
    $$PWD/include/simcardsiface.h

HEADERS_PRIVATE = \
    $$PWD/src/featuresiface_p.h \
    $$PWD/src/iudidiface_p.h \
    $$PWD/src/storagesiface_p.h \
    $$PWD/src/simcardsiface_p.h

HEADERS += \
    $$HEADERS_PUBLIC \
    $$HEADERS_PRIVATE

SOURCES += \
    $$PWD/src/featuresiface.cpp \
    $$PWD/src/iudidiface.cpp \
    $$PWD/src/storagesiface.cpp \
    $$PWD/src/simcardsiface.cpp
