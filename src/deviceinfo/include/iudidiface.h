// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef IUDIDIFACE_H
#define IUDIDIFACE_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>

class IudidIfacePrivate;

class IudidIface : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool available READ available CONSTANT)
    Q_PROPERTY(QString deviceId READ deviceId CONSTANT)

public:
    explicit IudidIface(QObject *parent = nullptr);
    ~IudidIface() override = default;

    bool available() const;

    QString deviceId() const;

private:
    QSharedPointer<IudidIfacePrivate> m_data{ nullptr };
};

#endif // IUDIDIFACE_H
