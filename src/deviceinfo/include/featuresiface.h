// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef FEATURESIFACE_H
#define FEATURESIFACE_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include <QtCore/QSize>

class FeaturesIfacePrivate;

class FeaturesIface : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool available READ available CONSTANT)

    Q_PROPERTY(QString osName READ osName CONSTANT)
    Q_PROPERTY(QString osVersion READ osVersion CONSTANT)
    Q_PROPERTY(QString deviceName READ deviceName CONSTANT)
    Q_PROPERTY(QString deviceModel READ deviceModel CONSTANT)
    Q_PROPERTY(quint64 ramTotalSize READ ramTotalSize CONSTANT)
    Q_PROPERTY(quint64 ramUsedSize READ ramUsedSize NOTIFY ramUsedSizeChanged)
    Q_PROPERTY(quint64 ramFreeSize READ ramFreeSize NOTIFY ramFreeSizeChanged)
    Q_PROPERTY(quint32 cpuClockSpeed READ cpuClockSpeed CONSTANT)
    Q_PROPERTY(quint32 cpuCoresNumber READ cpuCoresNumber CONSTANT)
    Q_PROPERTY(quint32 batteryCharge READ batteryCharge NOTIFY batteryChargeChanged)
    Q_PROPERTY(QSize screenResolution READ screenResolution CONSTANT)
    Q_PROPERTY(qreal backCameraResolution READ backCameraResolution CONSTANT)
    Q_PROPERTY(qreal frontCameraResolution READ frontCameraResolution CONSTANT)
    Q_PROPERTY(bool hasBluetooth READ hasBluetooth CONSTANT)
    Q_PROPERTY(bool hasGnss READ hasGnss CONSTANT)
    Q_PROPERTY(bool hasWlan READ hasWlan CONSTANT)
    Q_PROPERTY(bool hasNfc READ hasNfc CONSTANT)

public:
    explicit FeaturesIface(QObject *parent = nullptr);
    ~FeaturesIface() override = default;

    bool available() const;

    QString osName() const;
    QString osVersion() const;
    QString deviceName() const;
    QString deviceModel() const;
    quint64 ramTotalSize() const;
    quint64 ramUsedSize() const;
    quint64 ramFreeSize() const;
    quint32 cpuClockSpeed() const;
    quint32 cpuCoresNumber() const;
    quint32 batteryCharge() const;
    QSize screenResolution() const;
    qreal backCameraResolution() const;
    qreal frontCameraResolution() const;
    bool hasBluetooth() const;
    bool hasGnss() const;
    bool hasWlan() const;
    bool hasNfc() const;

signals:
    void ramUsedSizeChanged(quint64 ramUsedSize);
    void ramFreeSizeChanged(quint64 ramFreeSize);
    void batteryChargeChanged(quint32 batteryCharge);

private:
    QSharedPointer<FeaturesIfacePrivate> m_data{ nullptr };
};

#endif // FEATURESIFACE_H
