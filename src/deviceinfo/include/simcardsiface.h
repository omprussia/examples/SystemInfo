// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef SIMCARDSIFACE_H
#define SIMCARDSIFACE_H

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtCore/QString>

class SimCardsIfacePrivate;

class SimCardsIface : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool available READ available CONSTANT)

    Q_PROPERTY(qint32 simCardsCount READ simCardsCount NOTIFY simCardsCountChanged)
    Q_PROPERTY(
            qint32 simCardIndex READ simCardIndex WRITE setSimCardIndex NOTIFY simCardIndexChanged)
    Q_PROPERTY(QString simCardName READ simCardName NOTIFY simCardNameChanged)
    Q_PROPERTY(QString operatorName READ operatorName NOTIFY operatorNameChanged)
    Q_PROPERTY(bool simCardEnabled READ simCardEnabled NOTIFY simCardEnabledChanged)
    Q_PROPERTY(bool preferredDataTransfer READ preferredDataTransfer NOTIFY
                       preferredDataTransferChanged)
    Q_PROPERTY(bool preferredVoiceCall READ preferredVoiceCall NOTIFY preferredVoiceCallChanged)

public:
    explicit SimCardsIface(QObject *parent = nullptr);
    ~SimCardsIface() override = default;

    bool available() const;

    qint32 simCardsCount() const;
    qint32 simCardIndex() const;
    QString simCardName() const;
    QString operatorName() const;
    bool simCardEnabled() const;
    bool preferredDataTransfer() const;
    bool preferredVoiceCall() const;

signals:
    void simCardsCountChanged(qint32 simCardsCount);
    void simCardIndexChanged(qint32 simCardIndex);
    void simCardNameChanged(const QString &simCardName);
    void operatorNameChanged(const QString &operatorName);
    void simCardEnabledChanged(bool simCardEnabled);
    void preferredDataTransferChanged(bool preferredDataTransfer);
    void preferredVoiceCallChanged(bool preferredVoiceCall);

public slots:
    void setSimCardIndex(qint32 simCardIndex);

private:
    QSharedPointer<SimCardsIfacePrivate> m_data{ nullptr };
};

#endif // SIMCARDSIFACE_H
