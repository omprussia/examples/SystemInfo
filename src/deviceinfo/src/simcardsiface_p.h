// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef SIMCARDSIFACE_P_H
#define SIMCARDSIFACE_P_H

#include <QtCore/QObject>
#include <QtCore/QVariantMap>
#include <QtCore/QVector>

class QDBusInterface;

class SimCardsIfacePrivate : public QObject
{
    Q_OBJECT

public:
    explicit SimCardsIfacePrivate(QObject *parent = nullptr);
    ~SimCardsIfacePrivate();

    bool available() const;

    qint32 simCardsCount() const;
    qint32 simCardIndex() const;
    QString simCardName() const;
    QString operatorName() const;
    bool simCardEnabled() const;
    bool preferredDataTransfer() const;
    bool preferredVoiceCall() const;

signals:
    void simCardsCountChanged(qint32 simCardsCount);
    void simCardIndexChanged(qint32 simCardIndex);
    void simCardNameChanged(const QString &simCardName);
    void operatorNameChanged(const QString &operatorName);
    void simCardEnabledChanged(bool simCardEnabled);
    void preferredDataTransferChanged(bool preferredDataTransfer);
    void preferredVoiceCallChanged(bool preferredVoiceCall);

public slots:
    void setSimCardIndex(qint32 simCardIndex);

private slots:
    void updateAllData();
    void updateSimCardsInfo(const QList<QVariantMap> &simCardsInfo);

private:
    QDBusInterface *m_iface{ nullptr };

    qint32 m_simCardIndex{ -1 };
    QVector<QString> m_simCardName{};
    QVector<QString> m_operatorName{};
    QVector<bool> m_simCardEnabled{};
    QVector<bool> m_preferredDataTransfer{};
    QVector<bool> m_preferredVoiceCall{};
};

#endif // SIMCARDSIFACE_P_H
