// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef STORAGESIFACE_P_H
#define STORAGESIFACE_P_H

#include <QtCore/QObject>
#include <QtCore/QVariantMap>

class QDBusInterface;
class QTimer;

class StoragesIfacePrivate : public QObject
{
    Q_OBJECT

public:
    explicit StoragesIfacePrivate(QObject *parent = nullptr);
    ~StoragesIfacePrivate();

    bool available() const;

    bool extStorageMounted() const;
    QString extStorageDeviceLabel() const;
    quint32 extStoragePartitionsCount() const;
    quint64 extStorageBytesTotal() const;
    quint64 extStorageBytesUsed() const;
    quint64 extStorageBytesFree() const;

    QString intStorageDeviceLabel() const;
    QString intStorageFileSystemType() const;
    quint64 intStorageBytesTotal() const;
    quint64 intStorageBytesUsed() const;
    quint64 intStorageBytesFree() const;

    QString intUsrPartsDeviceLabel() const;
    QString intUsrPartsFileSystemType() const;
    quint64 intUsrPartsBytesTotal() const;
    quint64 intUsrPartsBytesUsed() const;
    quint64 intUsrPartsBytesFree() const;

signals:
    void extStorageMountedChanged(bool mounted);
    void extStorageDeviceLabelChanged(const QString &deviceLabel);
    void extStoragePartitionsCountChanged(quint32 partitionsCount);
    void extStorageBytesTotalChanged(quint64 bytesTotal);
    void extStorageBytesUsedChanged(quint64 bytesUsed);
    void extStorageBytesFreeChanged(quint64 bytesFree);

    void intStorageDeviceLabelChanged(const QString &deviceLabel);
    void intStorageFileSystemTypeChanged(const QString &fileSystemType);
    void intStorageBytesTotalChanged(quint64 bytesTotal);
    void intStorageBytesUsedChanged(quint64 bytesUsed);
    void intStorageBytesFreeChanged(quint64 bytesFree);

    void intUsrPartsDeviceLabelChanged(const QString &deviceLabel);
    void intUsrPartsFileSystemTypeChanged(const QString &fileSystemType);
    void intUsrPartsBytesTotalChanged(quint64 bytesTotal);
    void intUsrPartsBytesUsedChanged(quint64 bytesUsed);
    void intUsrPartsBytesFreeChanged(quint64 bytesFree);

private slots:
    void updateAllData();
    void updateExternalStorageInfo(const QVariantMap &externalStorageInfo);
    void updateInternalStorageInfo();
    void updateInternalUserPartitionInfo();

    void updateStorageAndUserPartitionInfo();

private:
    QTimer *m_timer{ nullptr };
    QDBusInterface *m_iface{ nullptr };

    bool m_extStorageMounted{ false };
    QString m_extStorageDeviceLabel{ QStringLiteral("") };
    quint32 m_extStoragePartitionsCount{ 0u };
    quint64 m_extStorageBytesTotal{ 0ull };
    quint64 m_extStorageBytesUsed{ 0ull };
    quint64 m_extStorageBytesFree{ 0ull };

    QString m_intStorageDeviceLabel{ QStringLiteral("") };
    QString m_intStorageFileSystemType{ QStringLiteral("") };
    quint64 m_intStorageBytesTotal{ 0ull };
    quint64 m_intStorageBytesUsed{ 0ull };
    quint64 m_intStorageBytesFree{ 0ull };

    QString m_intUsrPartsDeviceLabel{ QStringLiteral("") };
    QString m_intUsrPartsFileSystemType{ QStringLiteral("") };
    quint64 m_intUsrPartsBytesTotal{ 0ull };
    quint64 m_intUsrPartsBytesUsed{ 0ull };
    quint64 m_intUsrPartsBytesFree{ 0ull };
};

#endif // STORAGESIFACE_P_H
