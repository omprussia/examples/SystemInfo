// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "iudidiface_p.h"
#include "iudidiface.h"

namespace {
const QString DBUS_SERVICE = QStringLiteral("org.instance_unique_device_id");
const QString DBUS_PATH = QStringLiteral("/org/instance_unique_device_id");
const QString DBUS_IFACE = QStringLiteral("org.instance_unique_device_id");
}; // namespace

IudidIfacePrivate::IudidIfacePrivate(QObject *parent)
    : QObject(parent), m_iface()
{
    m_iface = new QDBusInterface(DBUS_SERVICE, DBUS_PATH, DBUS_IFACE, QDBusConnection::systemBus(),
                                 this);
    if (!m_iface->isValid()) {
        delete m_iface;
        // before Aurora OS 4.1.0:
        m_iface = new QDBusInterface(DBUS_SERVICE, DBUS_PATH, DBUS_IFACE,
                                     QDBusConnection::sessionBus(), this);
    }
    if (!m_iface->isValid()) {
        return;
    }

    updateAllData();
}

bool IudidIfacePrivate::available() const
{
    return m_iface->connection().interface()->isServiceRegistered(DBUS_SERVICE).value();
}

void IudidIfacePrivate::updateAllData()
{
    QDBusPendingReply<QString> deviceIdReply = m_iface->asyncCall(QStringLiteral("deviceId"));
    const QString deviceId = deviceIdReply.value();
    if (deviceId.isEmpty()) {
        m_deviceId = QStringLiteral("");
    } else {
        m_deviceId = deviceId;
    }
}

QString IudidIfacePrivate::deviceId() const
{
    return m_deviceId;
}

IudidIface:: IudidIface(QObject *parent)
    : QObject(parent), m_data(new  IudidIfacePrivate(this))
{ }

bool IudidIface::available() const
{
    return m_data->available();
}

QString IudidIface::deviceId() const
{
    return m_data->deviceId();
}
