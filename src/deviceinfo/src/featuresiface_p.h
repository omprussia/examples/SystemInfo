// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef FEATURES_P_H
#define FEATURES_P_H

#include <QtCore/QObject>
#include <QtCore/QSize>

class QDBusInterface;
class QTimer;

class FeaturesIfacePrivate : public QObject
{
    Q_OBJECT

public:
    explicit FeaturesIfacePrivate(QObject *parent = nullptr);
    ~FeaturesIfacePrivate();

    bool available() const;

    QString osName() const;
    QString osVersion() const;
    QString deviceName() const;
    QString deviceModel() const;
    quint64 ramTotalSize() const;
    quint64 ramUsedSize() const;
    quint64 ramFreeSize() const;
    quint32 cpuClockSpeed() const;
    quint32 cpuCoresCount() const;
    quint32 batteryCharge() const;
    QSize screenResolution() const;
    qreal backCameraResolution() const;
    qreal frontCameraResolution() const;
    bool hasBluetooth() const;
    bool hasGNSS() const;
    bool hasWlan() const;
    bool hasNFC() const;

signals:
    void ramUsedSizeChanged(quint64 ramUsedSize);
    void ramFreeSizeChanged(quint64 ramFreeSize);
    void batteryChargeChanged(quint32 batteryCharge);

private slots:
    void updateAllData();
    void updateRamFreeSize();
    void updateBatteryCharge();

    void updateRamAndBatteryInfo();

private:
    QTimer *m_timer{ nullptr };
    QDBusInterface *m_iface{ nullptr };

    QString m_osName{ QStringLiteral("") };
    QString m_osVersion{ QStringLiteral("") };
    QString m_deviceName{ QStringLiteral("") };
    QString m_deviceModel{ QStringLiteral("") };
    quint64 m_ramTotalSize{ 0ull };
    quint64 m_ramUsedSize{ 0ull };
    quint64 m_ramFreeSize{ 0ull };
    quint32 m_cpuClockSpeed{ 0u };
    quint32 m_cpuCoresCount{ 0u };
    quint32 m_batteryCharge{ 0u };
    QSize m_screenResolution{};
    qreal m_backCameraResolution{ 0.0 };
    qreal m_frontCameraResolution{ 0.0 };
    bool m_hasBluetooth{ false };
    bool m_hasGNSS{ false };
    bool m_hasWlan{ false };
    bool m_hasNFC{ false };
};

#endif // FEATURES_P_H
