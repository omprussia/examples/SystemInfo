// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef IUDIDIFACE_P_H
#define IUDIDIFACE_P_H

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusConnectionInterface>

class QDBusInterface;
class QTimer;

class IudidIfacePrivate : public QObject
{
    Q_OBJECT
public:
    explicit IudidIfacePrivate(QObject *parent = nullptr);
    ~IudidIfacePrivate() override = default;

    bool available() const;

    QString deviceId() const;

signals:

private slots:
    void updateAllData();

private:
    QDBusInterface *m_iface{ nullptr };

    QString m_deviceId{ QStringLiteral("") };
};

#endif // IUDIDIFACE_P_H
