// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PACKAGEINFOPROVIDER_H
#define PACKAGEINFOPROVIDER_H

#include <QObject>
#include <PackageKitAdaptor>

/*!
 * \brief Provides information about the package with the given identifier.
 */
class PackageInfoProvider : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString architecture READ architecture NOTIFY generalDataChanged)
    Q_PROPERTY(QString description READ description NOTIFY detailDataChanged)
    Q_PROPERTY(QString errorString READ errorString NOTIFY hasErrorChanged)
    Q_PROPERTY(bool hasError READ hasError NOTIFY hasErrorChanged)
    Q_PROPERTY(QString license READ license NOTIFY detailDataChanged)
    Q_PROPERTY(QString name READ name NOTIFY generalDataChanged)
    Q_PROPERTY(QString packageId READ packageId WRITE setPackageId NOTIFY packageIdChanged)
    Q_PROPERTY(QString size READ size NOTIFY detailDataChanged)
    Q_PROPERTY(QString status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString summary READ summary NOTIFY detailDataChanged)
    Q_PROPERTY(QString version READ version NOTIFY generalDataChanged)
    Q_PROPERTY(QString url READ url NOTIFY detailDataChanged)

public:
    explicit PackageInfoProvider(QObject *parent = nullptr);

    QString architecture() const { return m_architecture; }
    QString description() const { return m_description; }
    QString errorString() const { return m_errorString; }
    bool hasError() const { return m_hasError; }
    QString license() const { return m_license; }
    QString name() const { return m_name; }
    QString packageId() const { return m_packageId; }
    void setPackageId(QString packageId);
    QString size() const;
    QString status() const { return m_status; }
    QString summary() const { return m_summary; }
    QString version() const { return m_version; }
    QString url() const { return m_url; }

signals:
    void detailDataChanged();
    void hasErrorChanged();
    void generalDataChanged();
    void packageIdChanged();
    void statusChanged();

private:
    void _addPackageInfo(PackageKitAdaptor::Info info, const QString &packageID,
                         const QString &summary);
    void _processStoreTransactionCompletion(PackageKitAdaptor::Exit status, quint32 runtime);
    void _processResolveTransactionCompletion(PackageKitAdaptor::Exit status, quint32 runtime);
    void _processExitStatus(PackageKitAdaptor::Exit status);
    void _storePackageDetails(const PackageKitAdaptor::PackageInfo &values);

    QString m_architecture;
    QString m_description;
    QString m_errorString;
    bool m_hasError;
    QString m_license;
    QString m_name;
    QString m_packageId;
    PackageKitAdaptor::Transaction *m_searchTransaction;
    qulonglong m_size;
    QString m_status;
    QString m_summary;
    PackageKitAdaptor::Transaction *m_resolveTransaction;
    QString m_version;
    QString m_url;
};

#endif // PACKAGEINFOPROVIDER_H
