// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PKA_CONSTANTS_H
#define PKA_CONSTANTS_H

#include "global.h"

PKA_NAMESPACE_BEGIN

class Constants
{
public:
    static const QString dbusProperties;
    static const QString packageKitService;
    static const QString packageKitPath;
    static const char *packageKitIface;
    static const char *packageKitTransactionIface;

private:
    Constants() = delete;
};

PKA_NAMESPACE_END

#endif // PKA_CONSTANTS_H
