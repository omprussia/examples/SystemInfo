// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "daemon.h"
#include "daemon_p.h"
#include "transaction_p.h"
#include "constants_p.h"

PKA_NAMESPACE_USE

DaemonPrivate::DaemonPrivate(QObject *parent)
    : QDBusAbstractInterface(Constants::packageKitService, Constants::packageKitPath,
                             Constants::packageKitIface, QDBusConnection::systemBus(), parent)
{
    QDBusServiceWatcher *watcher = new QDBusServiceWatcher(Constants::packageKitService,
                                                           QDBusConnection::systemBus(),
                                                           QDBusServiceWatcher::WatchForOwnerChange, this);

    connect(watcher, &QDBusServiceWatcher::serviceOwnerChanged,
            this, [this] (const QString &service, const QString &oldOwner, const QString &newOwner) {
        Q_UNUSED(service)
        Q_UNUSED(oldOwner)

        if (!newOwner.isEmpty()) {
            _makeInfoRequest();

            if (!m_running) {
                m_running = true;
                emit runningChanged(m_running);
            }
        } else if (m_running) {
            m_running = false;
            emit runningChanged(m_running);
        }
    });

    _makeInfoRequest();
}

bool DaemonPrivate::running() const
{
    return m_running;
}

DaemonInfo DaemonPrivate::info() const
{
    return m_info;
}

Transaction *DaemonPrivate::details(const QStringList &packageIDs)
{
    TransactionPrivate *transactionPrivate = _makeTransactionPrivate();
    Transaction *transaction = new Transaction(transactionPrivate);
    transactionPrivate->GetDetails(packageIDs);
    return transaction;
}

Transaction *DaemonPrivate::resolve(const QStringList &packageNames, Filters filters)
{
    TransactionPrivate *transactionPrivate = _makeTransactionPrivate();
    Transaction *transaction = new Transaction(transactionPrivate);
    transactionPrivate->Resolve(filters, packageNames);
    return transaction;
}

Transaction *DaemonPrivate::searchDetails(const QStringList &search, Filters filters)
{
    TransactionPrivate *transactionPrivate = _makeTransactionPrivate();
    Transaction *transaction = new Transaction(transactionPrivate);
    transactionPrivate->SearchDetails(filters, search);
    return transaction;
}

void DaemonPrivate::_updateInfo(const QVariantMap &info)
{
    if (!m_running) {
        m_running = true;
        emit runningChanged(m_running);
    }

    if (m_info != info) {
        m_info = info;
        emit infoChanged(m_info);
    }
}

TransactionPrivate *DaemonPrivate::_makeTransactionPrivate()
{
    QDBusPendingReply<QDBusObjectPath> reply = call(QStringLiteral("CreateTransaction"));
    TransactionPrivate *transactionPrivate = new TransactionPrivate(reply.value().path(), parent());
    Q_ASSERT(transactionPrivate->isValid());
    return transactionPrivate;
}

void DaemonPrivate::_makeInfoRequest()
{
    QDBusMessage message = QDBusMessage::createMethodCall(Constants::packageKitService,
                                                          Constants::packageKitPath,
                                                          Constants::dbusProperties,
                                                          QStringLiteral("GetAll"));
    message << Constants::packageKitService;
    QDBusConnection::systemBus().callWithCallback(message, this, SLOT(_updateInfo(QVariantMap)));
}

Daemon *Daemon::m_instance = nullptr;

Daemon *Daemon::instance()
{
    if (m_instance == nullptr)
        m_instance = new Daemon(qApp);
    return m_instance;
}

bool Daemon::running()
{
    return instance()->m_data->running();
}

DaemonInfo Daemon::info()
{
    return instance()->m_data->info();
}

Transaction *Daemon::details(const QString &packageID)
{
    return instance()->m_data->details(QStringList(packageID));
}

Transaction *Daemon::details(const QStringList &packageIDs)
{
    return instance()->m_data->details(packageIDs);
}

Transaction *Daemon::resolve(const QString &packageName, Filters filters)
{
    return instance()->m_data->resolve(QStringList(packageName), filters);
}

Transaction *Daemon::resolve(const QStringList &packageNames, Filters filters)
{
    return instance()->m_data->resolve(packageNames, filters);
}

Transaction *Daemon::searchDetails(const QString &search, Filters filters)
{
    return instance()->m_data->searchDetails(QStringList(search), filters);
}

Transaction *Daemon::searchDetails(const QStringList &search, Filters filters)
{
    return instance()->m_data->searchDetails(search, filters);
}

Daemon::Daemon(QObject *parent)
    : QObject(parent), m_data(new DaemonPrivate(this))
{
    qRegisterMetaType<Filter>("Filter");
    qRegisterMetaType<Filters>("Filters");
    qRegisterMetaType<Group>("Group");
    qRegisterMetaType<Groups>("Groups");
    qRegisterMetaType<Role>("Role");
    qRegisterMetaType<Roles>("Roles");
    qRegisterMetaType<Network>("Network");
    qRegisterMetaType<Info>("Info");
    qRegisterMetaType<Error>("Error");
    qRegisterMetaType<Exit>("Exit");
    qRegisterMetaType<DaemonInfo>("DaemonInfo");
    qRegisterMetaType<PackageInfo>("PackageInfo");

    connect(m_data.data(), &DaemonPrivate::runningChanged,
            this, &Daemon::runningChanged);
    connect(m_data.data(), &DaemonPrivate::infoChanged,
            this, &Daemon::infoChanged);
}
