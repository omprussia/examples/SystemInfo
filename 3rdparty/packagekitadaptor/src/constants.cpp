// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "constants_p.h"

PKA_NAMESPACE_USE

const QString Constants::dbusProperties = QStringLiteral("org.freedesktop.DBus.Properties");
const QString Constants::packageKitService = QStringLiteral("org.freedesktop.PackageKit");
const QString Constants::packageKitPath = QStringLiteral("/org/freedesktop/PackageKit");
const char *Constants::packageKitIface = "org.freedesktop.PackageKit";
const char *Constants::packageKitTransactionIface = "org.freedesktop.PackageKit.Transaction";
