// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "transaction.h"
#include "transaction_p.h"
#include "constants_p.h"

PKA_NAMESPACE_USE

TransactionPrivate::TransactionPrivate(const QString &path, QObject *parent)
    : QDBusAbstractInterface(Constants::packageKitService, path,
                             Constants::packageKitTransactionIface,
                             QDBusConnection::systemBus(), parent)
{
}

void TransactionPrivate::GetDetails(const QStringList &packageIds)
{
    asyncCall("GetDetails", packageIds);
}

void TransactionPrivate::Resolve(quint64 filter, const QStringList &search)
{
    asyncCall("Resolve", filter, search);
}

void TransactionPrivate::SearchDetails(quint64 filter, const QStringList &search)
{
    asyncCall("SearchDetails", filter, search);
}

QString Transaction::packageName(const QString &packageID)
{
    return packageID.left(packageID.indexOf(QLatin1Char(';')));
}

QString Transaction::packageData(const QString &packageID)
{
    qint32 start = packageID.indexOf(QLatin1Char(';'));
    if (start == -1)
        return {  };

    start = packageID.indexOf(QLatin1Char(';'), ++start);
    if (start == -1)
        return {  };

    start = packageID.indexOf(QLatin1Char(';'), ++start);
    if (start == -1)
        return {  };

    return packageID.mid(++start);
}

QString Transaction::packageArch(const QString &packageID)
{
    qint32 start = packageID.indexOf(QLatin1Char(';'));
    if (start == -1)
        return {  };

    start = packageID.indexOf(QLatin1Char(';'), ++start);
    if (start == -1)
        return {  };

    qint32 end = packageID.indexOf(QLatin1Char(';'), ++start);
    return end == -1 ? packageID.mid(start) : packageID.mid(start, end - start);
}

QString Transaction::packageVersion(const QString &packageID)
{
    qint32 start = packageID.indexOf(QLatin1Char(';'));
    if (start == -1)
        return {  };

    qint32 end = packageID.indexOf(QLatin1Char(';'), ++start);
    return end == -1 ? packageID.mid(start) : packageID.mid(start, end - start);
}

Transaction::Transaction(TransactionPrivate *data)
    : m_data(data)
{
    if (!m_data.isNull()) {
        m_data->setParent(this);

        connect(m_data.data(), &TransactionPrivate::Package, [this](quint32 info, const QString &packageId, const QString &summary) {
            emit package(static_cast<Info>(info), packageId, summary);
        });
        connect(m_data.data(), &TransactionPrivate::Details,
                this, &Transaction::info);
        connect(m_data.data(), &TransactionPrivate::Finished, [this](quint32 exit, quint32 runtime) {
            emit finished(static_cast<Exit>(exit), runtime);
            this->deleteLater();
        });
        connect(m_data.data(), &TransactionPrivate::ErrorCode, [this](quint32 code, const QString &details) {
            emit errorCode(static_cast<Error>(code), details);
        });
    }
}
