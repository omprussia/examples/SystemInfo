// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "types.h"

PKA_NAMESPACE_USE

DaemonInfo::DaemonInfo()
    : QVariantMap()
{
}

DaemonInfo::DaemonInfo(const QVariantMap &details)
    : QVariantMap(details)
{
}

QString DaemonInfo::backendAuthor() const
{
    return value(QStringLiteral("BackendAuthor")).value<QString>();
}

QString DaemonInfo::backendDescription() const
{
    return value(QStringLiteral("BackendDescription")).value<QString>();
}

QString DaemonInfo::backendName() const
{
    return value(QStringLiteral("BackendName")).value<QString>();
}

QString DaemonInfo::distroId() const
{
    return value(QStringLiteral("DistroId")).value<QString>();
}

QStringList DaemonInfo::mimeTypes() const
{
    return value(QStringLiteral("MimeTypes")).value<QStringList>();
}

QVersionNumber DaemonInfo::version() const
{
    return QVersionNumber(value(QStringLiteral("VersionMajor")).value<qint32>(),
                          value(QStringLiteral("VersionMinor")).value<qint32>(),
                          value(QStringLiteral("VersionMicro")).value<qint32>());
}

Network DaemonInfo::network() const
{
    return static_cast<Network>(value(QStringLiteral("NetworkState")).value<quint32>());
}

Filters DaemonInfo::filters() const
{
    return static_cast<Filters>(value(QStringLiteral("Filters")).value<quint32>());
}

Groups DaemonInfo::groups() const
{
    return static_cast<Groups>(value(QStringLiteral("Groups")).value<quint64>());
}

Roles DaemonInfo::roles() const
{
    return static_cast<Roles>(value(QStringLiteral("Roles")).value<quint64>());
}

bool DaemonInfo::locked() const
{
    return value(QStringLiteral("Locked")).value<bool>();
}

PackageInfo::PackageInfo()
    : QVariantMap()
{
}

PackageInfo::PackageInfo(const QVariantMap &details)
    : QVariantMap(details)
{
}

QString PackageInfo::packageId() const
{
    return value(QStringLiteral("package-id")).value<QString>();
}

QString PackageInfo::description() const
{
    return value(QStringLiteral("description")).value<QString>();
}

QString PackageInfo::summary() const
{
    return value(QStringLiteral("summary")).value<QString>();
}

QString PackageInfo::license() const
{
    return value(QStringLiteral("license")).value<QString>();
}

QString PackageInfo::url() const
{
    return value(QStringLiteral("url")).value<QString>();
}

quint64 PackageInfo::size() const
{
    return value(QStringLiteral("size")).value<quint64>();
}

Group PackageInfo::group() const
{
    return static_cast<Group>(value(QStringLiteral("group")).value<quint32>());
}
