// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PKA_TRANSACTION_P_H
#define PKA_TRANSACTION_P_H

#include "types.h"

PKA_NAMESPACE_BEGIN

class TransactionPrivate final : public QDBusAbstractInterface
{
    Q_OBJECT

public:
    TransactionPrivate(const QString &path, QObject *parent = nullptr);

    void GetDetails(const QStringList &packageIds);
    void Resolve(quint64 filter, const QStringList &search);
    void SearchDetails(quint64 filter, const QStringList &search);

signals:
    void Package(quint32 info, const QString &packageId, const QString &summary);
    void Details(const QVariantMap &details);
    void Finished(quint32 exit, quint32 runtime);
    void ErrorCode(quint32 code, const QString &details);
};

PKA_NAMESPACE_END

#endif // PKA_TRANSACTION_H
