// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PKA_DAEMON_P_H
#define PKA_DAEMON_P_H

#include "types.h"
#include "transaction.h"

PKA_NAMESPACE_BEGIN

class DaemonPrivate final : public QDBusAbstractInterface
{
    Q_OBJECT

public:
    explicit DaemonPrivate(QObject *parent = nullptr);

    bool running() const;
    DaemonInfo info() const;

    Transaction *details(const QStringList &packageIDs);
    Transaction *resolve(const QStringList &packageNames, Filters filters = Filter::None);
    Transaction *searchDetails(const QStringList &search, Filters filters = Filter::None);

signals:
    void runningChanged(bool running);
    void infoChanged(const DaemonInfo &info);

private slots:
    void _updateInfo(const QVariantMap &info);

private:
    TransactionPrivate *_makeTransactionPrivate();
    void _makeInfoRequest();

private:
    bool m_running { false };
    DaemonInfo m_info {  };
};

PKA_NAMESPACE_END

#endif // PKA_DAEMON_H
