# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

QT += core dbus

INCLUDEPATH += $$PWD/include

HEADERS_PUBLIC = \
    $$PWD/include/global.h \
    $$PWD/include/bitfields.h \
    $$PWD/include/types.h \
    $$PWD/include/daemon.h \
    $$PWD/include/transaction.h

HEADERS_PRIVATE = \
    $$PWD/src/constants_p.h \
    $$PWD/src/daemon_p.h \
    $$PWD/src/transaction_p.h

HEADERS += \
    $$HEADERS_PUBLIC \
    $$HEADERS_PRIVATE

SOURCES += \
    $$PWD/src/constants.cpp \
    $$PWD/src/types.cpp \
    $$PWD/src/daemon.cpp \
    $$PWD/src/transaction.cpp

DISTFILES += \
    $$PWD/README.md \
    $$PWD/LICENSE.BSD-3-Clause.md
