# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = KackageKitLite
TEMPLATE = lib
DEFINES += PKL_LIBRARY
QT += core dbus

INCLUDEPATH += include

HEADERS_PUBLIC += \
    include/global.h \
    include/bitfields.h \
    include/types.h \
    include/daemon.h \
    include/transaction.h

HEADERS_PRIVATE += \
    src/constants_p.h \
    src/daemon_p.h \
    src/transaction_p.h

HEADERS += \
    $$HEADERS_PUBLIC \
    $$HEADERS_PRIVATE

SOURCES += \
    src/constants.cpp \
    src/types.cpp \
    src/daemon.cpp \
    src/transaction.cpp
    
DISTFILES += \
    README.md \
    LICENSE.BSD-3-Clause.md
