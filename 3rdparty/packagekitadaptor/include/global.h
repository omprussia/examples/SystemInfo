// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PKA_GLOBAL_H
#define PKA_GLOBAL_H

#include <QtCore>
#include <QtDBus>

#if defined(PKA_LIBRARY)
# define PKA_LIBRARY Q_DECL_EXPORT
#else
# define PKA_LIBRARY Q_DECL_IMPORT
#endif

#define PKA_NAMESPACE_NAME PackageKitAdaptor
#define PKA_NAMESPACE_BEGIN namespace PKA_NAMESPACE_NAME {
#define PKA_NAMESPACE_END }
#define PKA_NAMESPACE_USE using namespace PKA_NAMESPACE_NAME;

#endif // PKGLOBAL_H
