// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PKA_DAEMON_H
#define PKA_DAEMON_H

#include "types.h"
#include "transaction.h"

PKA_NAMESPACE_BEGIN

class DaemonPrivate;
class PKA_LIBRARY Daemon final : public QObject
{
    Q_OBJECT

public:
    static Daemon *instance();

    static bool running();
    static DaemonInfo info();

    static Transaction *details(const QString &packageID);
    static Transaction *details(const QStringList &packageIDs);
    static Transaction *resolve(const QString &packageName, Filters filters = Filter::None);
    static Transaction *resolve(const QStringList &packageNames, Filters filters = Filter::None);
    static Transaction *searchDetails(const QString &search, Filters filters = Filter::None);
    static Transaction *searchDetails(const QStringList &search, Filters filters = Filter::None);

signals:
    void runningChanged(bool running);
    void infoChanged(const DaemonInfo &info);

private:
    explicit Daemon(QObject *parent = nullptr);

private:
    static Daemon *m_instance;
    QSharedPointer<DaemonPrivate> m_data { nullptr };
};

PKA_NAMESPACE_END

#endif // PKA_DAEMON_H
