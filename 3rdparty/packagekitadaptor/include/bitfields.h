// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PKA_BITFIELDS_H
#define PKA_BITFIELDS_H

#include "global.h"

PKA_NAMESPACE_BEGIN

template<typename Enum>
class PKA_LIBRARY Bitfields final {
    static_assert((std::is_enum<Enum>::value), "Flag is only usable on enumeration types.");
public:
    inline Bitfields() : m_value(0ull) {  }
    inline Bitfields(quint64 value) : m_value(value) {  }

    inline Bitfields &operator&=(Enum mask) { m_value &= (1ull << static_cast<quint64>(mask)); return *this; }
    inline Bitfields &operator&=(Bitfields mask) { m_value &= mask.m_value; return *this; }
    inline Bitfields &operator|=(Enum mask) { m_value |= (1ull << static_cast<quint64>(mask)); return *this; }
    inline Bitfields &operator|=(Bitfields mask) { m_value |= mask.m_value; return *this; }

    inline Bitfields operator&(Enum mask) const { return Bitfields(m_value & (1ull << static_cast<quint64>(mask))); }
    inline Bitfields operator&(Bitfields mask) const { Bitfields(m_value & mask.m_value); }
    inline Bitfields operator|(Enum mask) const { return Bitfields(m_value | (1ull << static_cast<quint64>(mask))); }
    inline Bitfields operator|(Bitfields mask) const { return Bitfields(m_value | mask.m_value); }
    inline Bitfields operator~() const { return Bitfields(~m_value); }

    inline bool operator==(const Bitfields &other) { return m_value == other.m_value; }
    inline bool operator!() const { return !m_value; }
    inline operator quint64() const { return m_value; }

    inline bool testFlag(Enum flag) const { return static_cast<bool>(m_value & (1ull << static_cast<quint64>(flag))); }
    inline Bitfields &setFlag(Enum flag, bool on = true) { return on ? (*this |= flag) : (*this &= ~Bitfields(1ull << static_cast<quint64>(flag))); }

private:
    quint64 m_value { 0ull };
};

#define DECLARE_FLAGS(FlagsName, EnumName) using FlagsName = QFlags<EnumName>;
#define DECLARE_BITFIELDS(BitfieldsName, EnumName) using BitfieldsName = Bitfields<EnumName>;

PKA_NAMESPACE_END

#endif // PKA_BITFIELDS_H
