// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PKA_TRANSACTION_H
#define PKA_TRANSACTION_H

#include "types.h"

PKA_NAMESPACE_BEGIN

class DaemonPrivate;
class TransactionPrivate;
class PKA_LIBRARY Transaction final : public QObject
{
    Q_OBJECT

public:
    static QString packageName(const QString &packageID);
    static QString packageData(const QString &packageID);
    static QString packageArch(const QString &packageID);
    static QString packageVersion(const QString &packageID);

signals:
    void package(Info info, const QString &packageId, const QString &summary);
    void info(const PackageInfo &info);
    void finished(Exit exit, quint32 runtime);
    void errorCode(Error code, const QString &details);

private:
    Transaction(TransactionPrivate *data);

private:
    friend class DaemonPrivate;

    QSharedPointer<TransactionPrivate> m_data;
};

PKA_NAMESPACE_END

#endif // PKA_TRANSACTION_H
