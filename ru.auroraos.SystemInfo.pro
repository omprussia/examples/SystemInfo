# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.SystemInfo

CONFIG += \
    auroraapp \

QT += dbus

PKGCONFIG += \
    Qt5SystemInfo \

include (src/deviceinfo/deviceinfo.pri)
include (3rdparty/packagekitadaptor/packagekitadaptor.pri)

SOURCES += \
    src/cdbnetworkmanager.cpp \
    src/cdbservice.cpp \
    src/cdbtechnology.cpp \
    src/cdbtechnologymodel.cpp \
    src/main.cpp \
    src/storageinfomodel.cpp \
    src/packageinfoprovider.cpp \
    src/packagesearcher.cpp \

HEADERS += \
    src/cdbconstants.h \
    src/cdbnetworkmanager.h \
    src/cdbservice.h \
    src/cdbtechnology.h \
    src/cdbtechnologymodel.h \
    src/cdbtypes.h \
    src/cdbutils.h \
    src/storageinfomodel.h \
    src/packageinfoprovider.h \
    src/packagesearcher.h \

DISTFILES += \
    rpm/ru.auroraos.SystemInfo.spec \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-Clause.md \
    README.md \
    README.ru.md \
    ru.auroraos.SystemInfo.desktop

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.SystemInfo.ts \
    translations/ru.auroraos.SystemInfo-ru.ts \
