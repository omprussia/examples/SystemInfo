// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import QtMultimedia 5.5
import "../../components"
import "../../js/NetworkUiStrings.js" as NetworkServiceUI
import ru.omp.SystemInfo 1.0

ListViewPage {
    id: wlanInfoPage
    objectName: "wlanInfoPage"
    model: networkServiceModel
    delegate: itemDelegate

    CDBTechnologyModel {
        id: networkServiceModel
        objectName: "networkServiceModel"
        name: "wifi"
    }

    PullDownMenu {
        objectName: "pullDownMenu"
        quickSelect: true

        MenuItem {
            objectName: "updateItem"
            text: qsTr("Update")
            onClicked: networkServiceModel.requestScan()
        }
    }

    ViewPlaceholder {
        objectName: "placeholder"
        enabled: !networkServiceModel.powered || !networkServiceModel.available
        text: networkServiceModel.powered
                      ? qsTr("The WLAN is not available")
                      : qsTr("The WLAN is not powered")
        hintText: networkServiceModel.powered ? "" : qsTr("Turn the WLAN on")
    }

    Component {
        id: itemDelegate

        NetworkServiceItem {
            objectName: "wlanItem%1".arg(index)
            title: modelData.name
            networkService: modelData
        }
    }
}
