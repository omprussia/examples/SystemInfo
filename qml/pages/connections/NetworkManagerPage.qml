// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import "../../components"
import "../../js/UiStrings.js" as UiStrings
import "../../js/NetworkUiStrings.js" as NetworkUiStrings
import ru.omp.SystemInfo 1.0

ColumnPage {
    objectName: "networkManagerPage"
    bottomPadding: Theme.paddingLarge

    CDBNetworkManager {
        id: networkManager
        objectName: "networkManager"
    }

    InfoLabel {
        objectName: "notValidLabel"
        text: qsTr("Network manager is not available")
        visible: !networkManager.valid
    }

    Column {
        objectName: "validColumn"
        width: parent.width
        visible: networkManager.valid

        DetailItem {
            objectName: "availableItem"
            label: qsTr("Available")
            value: UiStrings.yesNo(networkManager.available)
        }

        DetailItem {
            objectName: "stateItem"
            label: qsTr("State")
            value: NetworkUiStrings.state(networkManager.state)
        }

        DetailItem {
            objectName: "connectedItem"
            label: qsTr("Connected")
            value: UiStrings.yesNo(networkManager.connected)
        }

        DetailItem {
            objectName: "connectingItem"
            label: qsTr("Connecting")
            value: UiStrings.yesNo(networkManager.connecting)
        }

        DetailItem {
            objectName: "connectingWifiItem"
            label: qsTr("Connecting Wi-Fi")
            value: UiStrings.yesNo(networkManager.connectingWifi)
        }

        DetailItem {
            objectName: "offlineModeItem"
            label: qsTr("Offline mode")
            value: UiStrings.yesNo(networkManager.offlineMode)
        }

        DetailItem {
            objectName: "sessionModeItem"
            label: qsTr("Session mode")
            value: UiStrings.yesNo(networkManager.sessionMode)
        }

        DetailItem {
            objectName: "inputRequestTimeoutItem"
            label: qsTr("Input request timeout")
            value: networkManager.inputRequestTimeout
        }

        DetailItem {
            objectName: "technologiesPowered"
            label: qsTr("Powered technologies")
            value: networkManager.technologies.join(", ") || qsTr("None")
        }

        NetworkServiceItem {
            objectName: "defaultRouteItem"
            title: qsTr("Default route")
            networkService: networkManager.defaultRoute
        }

        Loader {
            objectName: "connectedWifiLoader"
            active: networkManager.connectedWifi
                    && networkManager.connectedWifi.technicalName !== networkManager.defaultRoute.technicalName
                    && networkManager.connectedWifi.state === "online"

            sourceComponent: connectedWifiComponent
            width: parent.width

            onActiveChanged: if (!active) height = 0
        }
    }

    Component {
        id: connectedWifiComponent

        NetworkServiceItem {
            objectName: "connectedWifiItem"
            title: qsTr("Connected Wi-Fi")
            networkService: networkManager.connectedWifi
        }
    }
}
