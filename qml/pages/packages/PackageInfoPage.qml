// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.2
import Sailfish.Silica 1.0
import ru.omp.SystemInfo 1.0

Page {
    id: packageInfoPage

    objectName: "packageInfoPage"

    /* The package ID is passed from the package search page and
    installed into the PackageInfoProvider component. */
    property alias packageId: packageInfoProvider.packageId

    /**
     * A component providing detailed information about a package.
     */
    PackageInfoProvider {
        id: packageInfoProvider
        objectName: "packageInfoProvider"
    }

    /**
     * A component that displays information about a package as a list.
     */
    SilicaFlickable {
        objectName: "view"
        anchors {
            fill: parent
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
        }

        contentHeight: contentColumn.height

        Column {
            id: contentColumn

            objectName: "content"
            anchors {
                left: parent.left
                right: parent.right
            }

            PageHeader {
                objectName: "pageHeader"
                title: qsTr("Package information")
            }

            Column {
                objectName: "errorColumn"
                visible: packageInfoProvider.hasError
                spacing: Theme.paddingSmall
                anchors {
                    left: parent.left
                    right: parent.right
                }

                Label {
                    objectName: "errorLabel"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    color: Theme.highlightColor
                    text: packageInfoProvider.errorString
                }
            }

            Column {
                objectName: "infoColumn"
                visible: !packageInfoProvider.hasError
                spacing: Theme.paddingSmall
                anchors {
                    left: parent.left
                    right: parent.right
                }

                DetailItem {
                    objectName: "packageName"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("Name")
                    value: packageInfoProvider.name
                }

                DetailItem {
                    objectName: "packageVersion"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("Version")
                    value: packageInfoProvider.version
                }

                DetailItem {
                    objectName: "packageLicense"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("License")
                    value: packageInfoProvider.license
                }

                DetailItem {
                    objectName: "packageURL"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("URL")
                    value: packageInfoProvider.url
                }

                DetailItem {
                    objectName: "packageArchitecture"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("Architecture")
                    value: packageInfoProvider.architecture
                }

                DetailItem {
                    objectName: "packageStatus"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("Status")
                    value: packageInfoProvider.status
                }

                DetailItem {
                    objectName: "packageSize"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("Size")
                    value: packageInfoProvider.size
                }

                DetailItem {
                    objectName: "packageSummary"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    label: qsTr("Summary")
                    value: packageInfoProvider.summary
                }

                Label {
                    objectName: "packageDescriptionLabel"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    color: Theme.highlightColor
                    text: qsTr("Description")
                }

                Label {
                    objectName: "packageDescriptionText"
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    color: Theme.highlightColor
                    text: packageInfoProvider.description
                    wrapMode: Label.WrapAtWordBoundaryOrAnywhere
                    truncationMode: TruncationMode.None
                }
            }
        }
        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }
    }
}
