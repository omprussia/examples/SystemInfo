// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.DBus 2.0
import ru.omp.SystemInfo 1.0
import "../../components"
import "../../js/UiStrings.js" as UiStrings
import "../../js/DeviceInfoUiStrings.js" as DeviceInfoUiStrings

ColumnPage {
    objectName: "deviceInfoPage"
    bottomPadding: Theme.paddingLarge

    DeviceInfo {
        id: deviceInfo
        objectName: "deviceInfo"
    }

    IudidIface {
        id: iudidIface
        objectName: "iudidIface"
    }

    DetailItem {
        objectName: "productNameItem"
        label: qsTr("Product name")
        value: deviceInfo.productName()
    }

    DetailItem {
        objectName: "modelItem"
        label: qsTr("Model")
        value: deviceInfo.model()
    }

    DetailItem {
        objectName: "boardNameItem"
        label: qsTr("Board name")
        value: deviceInfo.boardName()
    }

    DetailItem {
        objectName: "manufacturerItem"
        label: qsTr("Manufacturer")
        value: deviceInfo.manufacturer()
    }

    DetailItem {
        objectName: "operatingSystemNameItem"
        label: qsTr("Operating system")
        value: deviceInfo.operatingSystemName()
    }

    DetailItem {
        objectName: "osVersionItem"
        label: qsTr("OS version")
        value: deviceInfo.version(DeviceInfo.Os)
    }

    DetailItem {
        objectName: "firmwareVersionItem"
        label: qsTr("Firmware version")
        value: deviceInfo.version(DeviceInfo.Firmware)
    }

    DetailItem {
        objectName: "uniqueDeviceIdItem"
        label: qsTr("Unique device ID")
        value: deviceInfo.uniqueDeviceID()
    }

    DetailItem {
        objectName: "iudidItem"
        label: qsTr("IUDID")
        value: iudidIface.deviceId
    }

    DetailItem {
        id: serialNumber
        objectName: "serialNumber"

        label: qsTr("Serial Number")

        DBusInterface {
            service: 'ru.omp.deviceinfo'
            path: '/ru/omp/deviceinfo/Features'
            iface: 'ru.omp.deviceinfo.Features'
            bus: DBus.SystemBus

            function getSerialNumber() {
                call('getSerialNumber',
                     undefined,
                     function(result) {
                         serialNumber.value = result
                     },
                     function(error, message) {
                         serialNumber.value = qsTr("Unknown")
                     })
            }

            Component.onCompleted: getSerialNumber()
        }
    }

    DetailItem {
        objectName: "thermalStateItem"
        label: qsTr("Thermal state")
        value: DeviceInfoUiStrings.thermalState(deviceInfo.thermalState)
    }

    DetailItem {
        objectName: "bluetoothFeatureItem"
        label: qsTr("Bluetooth available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.BluetoothFeature))
    }

    DetailItem {
        objectName: "currentBluetoothPowerStateItem"
        label: qsTr("Bluetooth enabled")
        value: UiStrings.yesNo(deviceInfo.currentBluetoothPowerState)
    }

    DetailItem {
        objectName: "cameraFeatureItem"
        label: qsTr("Camera available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.CameraFeature))
    }

    DetailItem {
        objectName: "fmRadioFeatureItem"
        label: qsTr("FM radio available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.FmRadioFeature))
    }

    DetailItem {
        objectName: "fmTransmitterFeatureItem"
        label: qsTr("FM transmitter available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.FmTransmitterFeature))
    }

    DetailItem {
        objectName: "infraredFeatureItem"
        label: qsTr("Infrared available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.InfraredFeature))
    }

    DetailItem {
        objectName: "ledFeatureItem"
        label: qsTr("LED available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.LedFeature))
    }

    DetailItem {
        objectName: "memoryCardFeatureItem"
        label: qsTr("Memory card slot available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.MemoryCardFeature))
    }

    DetailItem {
        objectName: "usbFeatureItem"
        label: qsTr("USB available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.UsbFeature))
    }

    DetailItem {
        objectName: "vibrationFeatureItem"
        label: qsTr("Vibration available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.VibrationFeature))
    }

    DetailItem {
        objectName: "wlanFeatureItem"
        label: qsTr("WLAN available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.WlanFeature))
    }

    DetailItem {
        objectName: "simFeatureItem"
        label: qsTr("SIM slot available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.SimFeature))
    }

    DetailItem {
        objectName: "positioningFeatureItem"
        label: qsTr("Positioning available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.PositioningFeature))
    }

    DetailItem {
        objectName: "videoOutFeatureItem"
        label: qsTr("Video output available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.VideoOutFeature))
    }

    DetailItem {
        objectName: "hapticsFeatureItem"
        label: qsTr("Haptics available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.HapticsFeature))
    }

    DetailItem {
        objectName: "nfcFeatureItem"
        label: qsTr("NFC available")
        value: UiStrings.yesNo(deviceInfo.hasFeature(DeviceInfo.NfcFeature))
    }
}
