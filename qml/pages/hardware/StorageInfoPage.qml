// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import ru.omp.SystemInfo 1.0
import "../../components"
import "../../js/UiStrings.js" as UiStrings
import "../../js/StorageInfoUiStrings.js" as StorageInfoUiStrings

ListViewPage {
    objectName: "storageInfoPage"
    model: storageInfoModel
    delegate: itemDelegate

    StorageInfoModel {
        id: storageInfoModel
        objectName: "storageInfoModel"
    }

    Component {
        id: itemDelegate

        Column {
            objectName: "storageItem%1".arg(index)
            width: parent.width
            bottomPadding: Theme.paddingLarge

            SectionHeader {
                objectName: "displayNameItem"
                text: model.displayName
            }

            DetailItem {
                objectName: "nameItem"
                label: qsTr("Name")
                value: model.name
            }

            DetailItem {
                objectName: "rootPathItem"
                label: qsTr("Path")
                value: model.rootPath
            }

            DetailItem {
                objectName: "deviceItem"
                label: qsTr("Device")
                value: model.device
            }

            DetailItem {
                objectName: "fileSystemTypeItem"
                label: qsTr("File system type")
                value: model.fileSystemType
            }

            DetailItem {
                objectName: "blockSizeItem"
                label: qsTr("Block size")
                value: model.blockSize
            }

            DetailItem {
                objectName: "bytesAvailableItem"
                label: qsTr("Available size")
                value: StorageInfoUiStrings.megabytes(model.bytesAvailable)
            }

            DetailItem {
                objectName: "bytesFreeItem"
                label: qsTr("Free size")
                value: StorageInfoUiStrings.megabytes(model.bytesFree)
            }

            DetailItem {
                objectName: "bytesTotalItem"
                label: qsTr("Total size")
                value: StorageInfoUiStrings.megabytes(model.bytesTotal)
            }

            DetailItem {
                objectName: "isReadOnlyItem"
                label: qsTr("Read only")
                value: UiStrings.yesNo(model.isReadOnly)
            }

            DetailItem {
                objectName: "isReadyItem"
                label: qsTr("Ready")
                value: UiStrings.yesNo(model.isReady)
            }

            DetailItem {
                objectName: "isRootItem"
                label: qsTr("Is root")
                value: UiStrings.yesNo(model.isRoot)
            }
        }
    }
}
