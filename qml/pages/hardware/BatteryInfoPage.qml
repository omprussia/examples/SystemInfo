// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.omp.SystemInfo 1.0
import "../../components"
import "../../js/UiStrings.js" as UiStrings
import "../../js/BatteryInfoUiStrings.js" as BatteryInfoUiStrings

ColumnPage {
    objectName: "batteryInfoPage"
    bottomPadding: Theme.paddingLarge

    BatteryInfo {
        id: batteryInfo
        objectName: "batteryInfo"
        batteryIndex: 1
    }

    Label {
        id: batterySelector

        objectName: "batterySelector"
        text: qsTr("Battery") + " " + 1
        width: parent.width
    }

    InfoLabel {
        objectName: "notValidLabel"
        text: qsTr("Battery info is not available")
        visible: !batteryInfo.valid
    }

    Column {
        objectName: "validColumn"
        width: parent.width
        visible: batteryInfo.valid

        DetailItem {
            objectName: "levelItem"
            label: qsTr("Level")
            value: qsTr("%L1 %").arg(batteryInfo.level)
        }

        DetailItem {
            objectName: "levelStatusItem"
            label: qsTr("Level status")
            value: BatteryInfoUiStrings.levelStatus(batteryInfo.levelStatus)
        }

        DetailItem {
            objectName: "voltageItem"
            label: qsTr("Voltage")
            value: batteryInfo.voltage >= 0
                   ? qsTr("%L1 mV").arg(batteryInfo.voltage)
                   : qsTr("Unknown")

        }

        DetailItem {
            objectName: "healthItem"
            label: qsTr("Health")
            value: BatteryInfoUiStrings.health(batteryInfo.health)
        }

        DetailItem {
            objectName: "maximumCapacityItem"
            label: qsTr("Maximum capacity")
            value: batteryInfo.maximumCapacity >= 0
                    ? qsTr("%L1 mAh").arg(batteryInfo.maximumCapacity)
                    : qsTr("Unknown")

        }

        DetailItem {
            objectName: "remainingCapacityItem"
            label: qsTr("Remaining capacity")
            value: batteryInfo.remainingCapacity >= 0
                   ? qsTr("%L1 mAh").arg(batteryInfo.remainingCapacity)
                   : qsTr("Unknown")
        }

        DetailItem {
            objectName: "cycleCountItem"
            label: qsTr("Cycle count")
            value: batteryInfo.cycleCount >= 0
                   ? batteryInfo.cycleCount
                   : qsTr("Unknown")
        }

        DetailItem {
            objectName: "temperatureItem"
            label: qsTr("Temperature")
            value: isNaN(batteryInfo.temperature)
                   ? qsTr("Unknown")
                   : qsTr("%L1 °C").arg(batteryInfo.temperature)
        }

        DetailItem {
            objectName: "chargingStateItem"
            label: qsTr("Charging state")
            value: BatteryInfoUiStrings.chargingState(batteryInfo.chargingState)
        }

        DetailItem {
            objectName: "chargerTypeItem"
            label: qsTr("Charger type")
            value: BatteryInfoUiStrings.chargerType(batteryInfo.chargerType)
        }

        DetailItem {
            objectName: "currentFlowItem"
            label: qsTr("Current flow")
            value: qsTr("%L1 mA").arg(batteryInfo.currentFlow)
        }

        DetailItem {
            objectName: "remainingChargingTimeItem"
            label: qsTr("Remaining charging time")
            value: batteryInfo.remainingChargingTime >= 0
                   ? qsTr("%L1 sec").arg(batteryInfo.remainingChargingTime)
                   : qsTr("Unknown")
        }
    }
}
