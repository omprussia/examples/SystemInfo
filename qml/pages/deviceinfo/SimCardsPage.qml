// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.omp.SystemInfo 1.0
import "../../components"
import "../../js/UiStrings.js" as UiStrings

ColumnPage {
    objectName: "simCardsPage"
    bottomPadding: Theme.paddingLarge

    SimCardsIface {
        id: simCardsIface
        objectName: "simCardsIface"
        simCardIndex: simCardSelector.currentIndex
    }

    ComboBox {
        id: simCardSelector

        objectName: "simCardSelector"
        label: qsTr("SIM card")
        width: parent.width
        enabled: simCardsIface.simCardsCount > 1

        menu: ContextMenu {
            objectName: "menu"

            Repeater {
                model: simCardsIface.simCardsCount
                delegate: MenuItem {
                    objectName: "item%1".arg(model.index)
                    text: model.index + 1
                }
            }
        }
    }

    InfoLabel {
        objectName: "notValidLabel"
        text: qsTr("SIM cards info is not available")
        visible: !simCardsIface.available
    }

    Column {
        objectName: "validColumn"
        width: parent.width
        visible: simCardsIface.available

        DetailItem {
            objectName: "simCardNameItem"
            label: qsTr("SIM card name")
            value: simCardsIface.simCardName
        }

        DetailItem {
            objectName: "operatorNameItem"
            label: qsTr("Operator name")
            value: simCardsIface.operatorName
        }

        DetailItem {
            objectName: "simCardEnabledItem"
            label: qsTr("SIM card enabled")
            value: UiStrings.yesNo(simCardsIface.simCardEnabled)
        }

        DetailItem {
            objectName: "preferredVoiceCallItem"
            label: qsTr("Preferred voice call")
            value: UiStrings.yesNo(simCardsIface.preferredVoiceCall)
        }

        DetailItem {
            objectName: "preferredDataTransferItem"
            label: qsTr("Preferred data transfer")
            value: UiStrings.yesNo(simCardsIface.preferredDataTransfer)
        }
    }
}
