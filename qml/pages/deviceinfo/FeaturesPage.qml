// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.omp.SystemInfo 1.0
import "../../components"
import "../../js/UiStrings.js" as UiStrings
import "../../js/StorageInfoUiStrings.js" as StorageInfoUiStrings

ColumnPage {
    objectName: "featuresPage"
    bottomPadding: Theme.paddingLarge

    FeaturesIface {
        id: featuresIface
        objectName: "featuresIface"
    }

    InfoLabel {
        objectName: "notValidLabel"
        text: qsTr("Features are not available")
        visible: !featuresIface.available
    }

    Column {
        objectName: "validColumn"
        width: parent.width
        visible: featuresIface.available

        DetailItem {
            objectName: "osItem"
            label: qsTr("OS")
            value: qsTr("%1 %2").arg(featuresIface.osName).arg(featuresIface.osVersion)
        }

        DetailItem {
            objectName: "deviceItem"
            label: qsTr("Device")
            value: qsTr("%1 %2").arg(featuresIface.deviceName).arg(featuresIface.deviceModel)
        }

        DetailItem {
            objectName: "ramTotalSizeItem"
            label: qsTr("RAM total size")
            value: StorageInfoUiStrings.megabytes(featuresIface.ramTotalSize)
        }

        DetailItem {
            objectName: "ramUsedSizeItem"
            label: qsTr("RAM used size")
            value: StorageInfoUiStrings.megabytes(featuresIface.ramUsedSize)
        }

        DetailItem {
            objectName: "ramFreeSizeItem"
            label: qsTr("RAM free size")
            value: StorageInfoUiStrings.megabytes(featuresIface.ramFreeSize)
        }

        DetailItem {
            objectName: "cpuClockSpeedItem"
            label: qsTr("CPU clock speed")
            value: qsTr("%1 Hz").arg(featuresIface.cpuClockSpeed)
        }

        DetailItem {
            objectName: "cpuCoresNumberItem"
            label: qsTr("CPU cores number")
            value: featuresIface.cpuCoresNumber
        }

        DetailItem {
            objectName: "batteryChargeItem"
            label: qsTr("Battery charge")
            value: qsTr("%1%").arg(featuresIface.batteryCharge)
        }

        DetailItem {
            objectName: "screenResolutionItem"
            label: qsTr("Screen resolution")
            value: qsTr("%1x%2").arg(featuresIface.screenResolution.width).arg(featuresIface.screenResolution.height)
        }

        DetailItem {
            objectName: "backCameraResolutionItem"
            label: qsTr("Back camera res.")
            value: qsTr("%1 MP").arg(featuresIface.backCameraResolution)
        }

        DetailItem {
            objectName: "frontCameraResolutionItem"
            label: qsTr("Front camera res.")
            value: qsTr("%1 MP").arg(featuresIface.frontCameraResolution)
        }

        DetailItem {
            objectName: "hasBluetoothItem"
            label: qsTr("Has Bluetooth")
            value: UiStrings.yesNo(featuresIface.hasBluetooth)
        }

        DetailItem {
            objectName: "hasGnssItem"
            label: qsTr("Has GNSS")
            value: UiStrings.yesNo(featuresIface.hasGnss)
        }

        DetailItem {
            objectName: "hasWlanItem"
            label: qsTr("Has WLAN")
            value: UiStrings.yesNo(featuresIface.hasWlan)
        }

        DetailItem {
            objectName: "hasNfcItem"
            label: qsTr("Has NFC")
            value: UiStrings.yesNo(featuresIface.hasNfc)
        }
    }
}
