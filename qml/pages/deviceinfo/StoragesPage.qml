// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.omp.SystemInfo 1.0
import "../../components"
import "../../js/UiStrings.js" as UiStrings
import "../../js/StorageInfoUiStrings.js" as StorageInfoUiStrings

ColumnPage {
    objectName: "storagesPage"
    bottomPadding: Theme.paddingLarge

    StoragesIface {
        id: storagesIface
        objectName: "storagesIface"
    }

    InfoLabel {
        objectName: "notValidLabel"
        text: qsTr("Storages info is not available")
        visible: !storagesIface.available
    }

    Column {
        objectName: "validColumn"
        width: parent.width
        visible: storagesIface.available

        Column {
            objectName: "intStorageColumn"
            width: parent.width

            SectionHeader {
                objectName: "intStorageItem"
                text: qsTr("Internal storage")
            }

            DetailItem {
                objectName: "intStorageDeviceLabelItem"
                label: qsTr("Device label")
                value: storagesIface.intStorageDeviceLabel
            }

            DetailItem {
                objectName: "intStorageFileSystemTypeItem"
                label: qsTr("File system type")
                value: storagesIface.intStorageFileSystemType
            }

            DetailItem {
                objectName: "intStorageBytesTotalItem"
                label: qsTr("Total size")
                value: StorageInfoUiStrings.megabytes(storagesIface.intStorageBytesTotal)
            }

            DetailItem {
                objectName: "intStorageBytesUsedItem"
                label: qsTr("Used size")
                value: StorageInfoUiStrings.megabytes(storagesIface.intStorageBytesUsed)
            }

            DetailItem {
                objectName: "intStorageBytesFreeItem"
                label: qsTr("Free size")
                value: StorageInfoUiStrings.megabytes(storagesIface.intStorageBytesFree)
            }
        }

        Column {
            objectName: "intUsrPartsColumn"
            width: parent.width

            SectionHeader {
                objectName: "intUsrPartsItem"
                text: qsTr("Internal user partition")
            }

            DetailItem {
                objectName: "intUsrPartsDeviceLabelItem"
                label: qsTr("Device label")
                value: storagesIface.intUsrPartsDeviceLabel
            }

            DetailItem {
                objectName: "intUsrPartsFileSystemTypeItem"
                label: qsTr("File system type")
                value: storagesIface.intUsrPartsFileSystemType
            }

            DetailItem {
                objectName: "intUsrPartsBytesTotalItem"
                label: qsTr("Total size")
                value: StorageInfoUiStrings.megabytes(storagesIface.intUsrPartsBytesTotal)
            }

            DetailItem {
                objectName: "intUsrPartsBytesUsedItem"
                label: qsTr("Used size")
                value: StorageInfoUiStrings.megabytes(storagesIface.intUsrPartsBytesUsed)
            }

            DetailItem {
                objectName: "intUsrPartsBytesFreeItem"
                label: qsTr("Free size")
                value: StorageInfoUiStrings.megabytes(storagesIface.intUsrPartsBytesFree)
            }
        }

        Column {
            objectName: "extStorageColumn"
            width: parent.width
            visible: storagesIface.extStorageMounted

            SectionHeader {
                objectName: "extStorageItem"
                text: qsTr("External storage")
            }

            DetailItem {
                objectName: "extStorageDeviceLabelItem"
                label: qsTr("Device label")
                value: storagesIface.extStorageDeviceLabel
            }

            DetailItem {
                objectName: "extStoragePartitionsCountItem"
                label: qsTr("Partitions count")
                value: storagesIface.extStoragePartitionsCount
            }

            DetailItem {
                objectName: "extStorageBytesTotalItem"
                label: qsTr("Total size")
                value: StorageInfoUiStrings.megabytes(storagesIface.extStorageBytesTotal)
            }

            DetailItem {
                objectName: "extStorageBytesUsedItem"
                label: qsTr("Used size")
                value: StorageInfoUiStrings.megabytes(storagesIface.extStorageBytesUsed)
            }

            DetailItem {
                objectName: "extStorageBytesFreeItem"
                label: qsTr("Free size")
                value: StorageInfoUiStrings.megabytes(storagesIface.extStorageBytesFree)
            }
        }
    }
}
