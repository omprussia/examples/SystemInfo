// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

.import ru.omp.SystemInfo.DeviceInfo 1.0 as DeviceInfo

/**
 * Returns the names of the device lock types.
 *
 * @param {number} value the lock type mask.
 * @returns {string} comma separated lock type names.
 */
function locks(value) {
    return _deviceLockStrings.filter(function(lockString, lockId) {
        return value & lockId
    }).join(", ") || _deviceLockStrings[DeviceInfo.NoLock]
}
var _deviceLockStrings = []
_deviceLockStrings[DeviceInfo.NoLock] = qsTr("No lock")
_deviceLockStrings[DeviceInfo.PinLock] = qsTr("PIN lock")
_deviceLockStrings[DeviceInfo.TouchOrKeyboardLock] = qsTr("Touch or keyboard lock")

/**
 * Returns the name of the device thermal state.
 *
 * @param {number} value the thermal state ID.
 * @returns {string}
 */
function thermalState(value) {
    return _thermalStateStrings[value] || qsTr("Unknown")
}
var _thermalStateStrings = []
_thermalStateStrings[DeviceInfo.NormalThermal] = qsTr("Normal")
_thermalStateStrings[DeviceInfo.WarningThermal] = qsTr("Warning")
_thermalStateStrings[DeviceInfo.AlertThermal] = qsTr("Alert")
_thermalStateStrings[DeviceInfo.ErrorThermal] = qsTr("Error")
