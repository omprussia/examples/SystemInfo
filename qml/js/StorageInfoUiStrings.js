// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

/**
 * Returns a bytes size in megabytes.
 *
 * @param {number} value the size in the bytes.
 * @returns {string}
 */
function megabytes(value) {
    return qsTr("%L1 MB").arg(value / 1e6)
}
