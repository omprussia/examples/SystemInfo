// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

.import ru.omp.SystemInfo.BatteryInfo 1.0 as BatteryInfo

/**
 * Returns the name of a charger type.
 *
 * @param {number} value the charger type ID
 * @returns {string}
 */
function chargerType(value) {
    return _chargerTypes[value] || qsTr("Unknown")
}
var _chargerTypes = []
_chargerTypes[BatteryInfo.WallCharger] = qsTr("Wall charger")
_chargerTypes[BatteryInfo.USBCharger] = qsTr("USB charger")
_chargerTypes[BatteryInfo.VariableCurrentCharger] = qsTr("Variable current charger")

/**
 * Returns the name of a battery charging state.
 *
 * @param {number} value the charging state ID.
 * @returns {string}
 */
function chargingState(value) {
    return _chargingStates[value] || qsTr("Unknown")
}
var _chargingStates = []
_chargingStates[BatteryInfo.Charging] = qsTr("Charging")
_chargingStates[BatteryInfo.IdleChargingState] = qsTr("Idle")
_chargingStates[BatteryInfo.Discharging] = qsTr("Discharging")

/**
 * Returns the name of a battery health status.
 *
 * @param {number} value the health status ID.
 * @returns {string}
 */
function health(value) {
    return _healths[value] || qsTr("Unknown")
}
var _healths = []
_healths[BatteryInfo.HealthBad] = qsTr("Bad")
_healths[BatteryInfo.HealthOk] = qsTr("Ok")

/**
 * Returns the name of a battery charge level.
 *
 * @param {number} value the charge level ID.
 * @returns {string}
 */
function levelStatus(value) {
    return _levelStatuses[value] || qsTr("Unknown")
}
var _levelStatuses = []
_levelStatuses[BatteryInfo.LevelEmpty] = qsTr("Empty")
_levelStatuses[BatteryInfo.LevelLow] = qsTr("Low")
_levelStatuses[BatteryInfo.LevelOk] = qsTr("Ok")
_levelStatuses[BatteryInfo.LevelFull] = qsTr("Full")
