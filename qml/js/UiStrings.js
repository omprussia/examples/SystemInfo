// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

/**
 * Convertsa a bool value into semantics.
 *
 * @param {bool} value true or false
 * @returns {string} yes for true and no for false.
 */
function yesNo(value) {
    return value ? qsTr("Yes") : qsTr("No")
}
