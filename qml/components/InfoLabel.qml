// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0

Label {
    objectName: "infoLabel"
    wrapMode: Text.Wrap
    horizontalAlignment: Text.AlignHCenter
    color: palette.secondaryHighlightColor
    font {
        pixelSize: Theme.fontSizeExtraLarge
        family: Theme.fontFamilyHeading
    }
    anchors {
        left: parent.left
        right: parent.right
        margins: Theme.horizontalPageMargin
    }
}
