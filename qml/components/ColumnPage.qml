// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    default property alias _: column.data
    property alias bottomPadding: column.bottomPadding
    property alias description: pageHeader.description
    property alias leftPadding: column.leftPadding
    property alias padding: column.padding
    property alias rightPadding: column.rightPadding
    property alias spacing: column.spacing
    property alias title: pageHeader.title
    property alias topPadding: column.topPadding

    objectName: "columnPage"
    allowedOrientations: Orientation.All

    SilicaFlickable {
        objectName: "flickable"
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column

            objectName: "column"
            width: parent.width

            PageHeader {
                id: pageHeader
                objectName: "pageHeader"
            }
        }

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }
    }
}
