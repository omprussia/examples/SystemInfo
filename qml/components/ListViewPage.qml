// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: listViewPage

    default property alias _: view.data
    property alias delegate: view.delegate
    property string description
    property list<Item> headerExtraContent
    property alias model: view.model
    property alias section: view.section
    property alias spacing: view.spacing
    property string title

    objectName: "listViewPage"
    allowedOrientations: Orientation.All

    SilicaListView {
        id: view

        objectName: "view"
        header: headerDelegate
        anchors.fill: parent

        VerticalScrollDecorator {
            objectName: "scrollDecorator"
        }
    }

    Component {
        id: headerDelegate

        PageHeader {
            objectName: "pageHeader"
            title: listViewPage.title
            description: listViewPage.description
            extraContent.children: listViewPage.headerExtraContent
        }
    }
}
