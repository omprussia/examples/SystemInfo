# Информация о системе

Проект содержит примеры использования API, который предоставляет информацию о среде
выполнения: ОС и устройстве.

Основная цель - показать не только то, какую информацию можно получить,
но и то, какие способы ее получения являются правильными.

Статус сборки:
1. example - [![pipeline status](https://gitlab.com/omprussia/examples/SystemInfo/badges/example/pipeline.svg)](https://gitlab.com/omprussia/examples/SystemInfo/-/commits/example)
2. dev - [![pipeline status](https://gitlab.com/omprussia/examples/SystemInfo/badges/dev/pipeline.svg)](https://gitlab.com/omprussia/examples/SystemInfo/-/commits/dev)

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта

Проект имеет стандартную структуру приложения на базе C++ и QML для ОС Аврора.

* Файл **[ru.auroraos.SystemInfo.pro](ru.auroraos.SystemInfo.pro)** описывает структуру проекта для системы сборки qmake.
* Каталог **[icons](icons)** содержит иконки приложения для поддерживаемых разрешений экрана.
* Каталог **[qml](qml)** содержит исходный код на QML и ресурсы интерфейса пользователя.
    * Каталог **[cover](qml/cover)** содержит реализации обложек приложения.
    * Каталог **[components](qml/components)** содержит пользовательские компоненты пользовательского интерфейса.
    * Каталог **[icons](qml/icons)** содержит дополнительные иконки интерфейса пользователя.
    * Каталог **[pages](qml/pages)** содержит страницы приложения.
    * Каталог **[js](qml/js)** содержит скрипты для конвертации пользовательских типов данных в строки.
    * Файл **[SystemInfo.qml](qml/SystemInfo.qml)** предоставляет реализацию окна приложения.
* Каталог **[rpm](rpm)** содержит настройки сборки rpm-пакета.
    * Файл **[ru.auroraos.SystemInfo.spec](rpm/ru.auroraos.SystemInfo.spec)** используется инструментом rpmbuild.
* Каталог **[src](src)** содержит исходный код на C++.
    * Файл **[main.cpp](src/main.cpp)** является точкой входа в приложение.
* Каталог **[translations](translations)** содержит файлы перевода интерфейса пользователя.
* Файл **[ru.auroraos.SystemInfo.desktop](ru.auroraos.SystemInfo.desktop)** определяет отображение и параметры запуска приложения.

## Совместимость

Проект совместим с актуальными версиями ОС Аврора.
  
## Снимки экранов

![screenshots](screenshots/screenshots.png)

## Примеры

### Hardware

Данная категория примеров посвящена получению информации о свойствах и компонентах устройства.
Соответствующие страницы с примерами находятся в директории **[hardware](qml/pages/hardware)**.

[QtSystemInfo](https://github.com/sailfishos/qtsystems)
позволяет получить общую информацию об устройстве.

Для использования библиотеки необходимо провести предварительную подготовку:

* Добавьте `BuildRequires: pkgconfig(Qt5SystemInfo)` в файл **.spec**.
* Добавьте `Qt5SystemInfo` в список `PKGCONFIG` файла **.pro**.

#### Информация об устройстве

Этот пример показывает как использовать
[QDeviceInfo](https://github.com/sailfishos/qtsystems/blob/mer-dev/src/systeminfo/qdeviceinfo.h)
для получения свойств устройства и сборки ОС.
Класс `QDeviceInfo` зарегистрирован как QML-тип `DeviceInfo`.

* **[DeviceInfoPage.qml](qml/pages/hardware/DeviceInfoPage.qml)** - страница примера.

#### Информация о хранилище

Этот пример показывает как использовать [QStorageInfo](https://doc.qt.io/archives/qt-5.6/qstorageinfo.html)
для получения свойств примонтированных томов.
Чтобы получить доступ к классу `QStorageInfo`, необходимо подключить заголовочный файл библиотеки.
Если в проекте не используется библиотека `QtSystemInfo`, то достаточно подключить заголовочный файл класса `QStorageInfo`.
Проблема вызвана наличием двух различных интерфейсов у класса с именем `QStorageInfo`.
В данном проекте используется класс Qt Core `QStorageInfo`.

* **[StorageInfoPage.qml](qml/pages/hardware/StorageInfoPage.qml)** - страница примера.
* **[storageinfomodel.h](src/storageinfomodel.h)** и **[storageinfomodel.cpp](src/storageinfomodel.cpp)**
    реализуют модель, которая предоставляет информацию о примонтированных томах.
    
### Соединения

Данная категория примеров посвящена получению информации о соединениях устройств.
Соответствующие страницы с примерами находятся в директории **[connections](qml/pages/connections)**.

[Connman](https://github.com/sailfishos/libconnman-qt)
это менеджер Интернет-соединений, который, в частности, позволяет получать информацию о сетевых соединениях, 
используя для этого DBus-сервис.

#### Состояние сети

В данном примере показано, что делать для получения статуса сетевых соединений.

* **[NetworkManagerPage.qml](qml/pages/connections/NetworkManagerPage.qml)** - страница примера.

### Пакеты

Эта категория примеров посвящена информации о пакетах устройства.
Соответствующие страницы с примерами находятся в директории **[packages](qml/pages/packages)**.

#### Информация о пакете

В данном примере показано, как с помощью [PackageKitAdaptor](#packagekitadaptor)
получить подробную информации о пакете.

* **[PackageInfoPage.qml](qml/pages/packages/PackageInfoPage.qml)** - страница примера.

#### Поиск пакетов

В данном примере показано, как с помощью [PackageKitAdaptor](#packagekitadaptor)
получить список пакетов устройства.

* **[PackageSearchPage.qml](qml/pages/packages/PackageSearchPage.qml)** - страница примера.

#### PackageKitAdaptor

Чтобы получить информацию об установленных на устройстве пакетах, воспользуйтесь
системой PackageKit. Для работы с этой системой был реализован DBus-адаптер.
Он называется PackageKitAdaptor и может быть подключен как подпроект или как библиотека.
- Чтобы подключить его как подпроект, достаточно добавить его в свой pro-файл:
    `include (3rdparty/packagekitadaptor/packagekitadaptor.pri)`.
- Чтобы подключить его как библиотеку, то необходимо собрать проект:
    `include (3rdparty/packagekitadaptor/packagekitadaptor.pro)`,
    и подключить его в свой pro-файл стандартным способом.

В состав библиотеки входят классы **[Demon](3rdparty/packagekitadaptor/daemon.h)** и
**[Transaction](3rdparty/packagekitadaptor/transaction.h)**, которые предоставляют информацию
о пакетах. В C++ коде следует использовать систему управления пакетами.

В данном приложении `Daemon` и `Transaction` используются в составе
**[PackageSearcher](src/packagesearcher.h)** и **[PackageInfoProvider](src/packageinfoprovider.h)**.
Они регистрируются как QML-типы, которые используются на страницах **[PackagesSearchPage.qml](qml/pages/packages/PackagesSearchPage.qml)**
и **[PackageInfoPage.qml](qml/pages/packages/PackageInfoPage.qml)** для поиска необходимых
пакетов и генерации подробной информации о них.

## This document in English

- [README.md](README.md)
