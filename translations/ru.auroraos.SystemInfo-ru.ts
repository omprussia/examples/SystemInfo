<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="35"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Проект содержит примеры использования API, который предоставляет информацию о среде
выполнения: ОС и устройстве.&lt;/p&gt;

&lt;p&gt;Основная цель - показать не только то, какую информацию можно получить,
но и то, какие способы ее получения являются правильными.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="26"/>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="46"/>
        <source>The 3-Clause BSD License</source>
        <translation>The 3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="60"/>
        <source>&lt;p&gt;&lt;em&gt;Copyright (c) 2021-2022 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;em&gt;Copyright (c) 2021-2022 Open Mobile Platform LLC&lt;/em&gt;&lt;/p&gt;
                            &lt;p&gt;Redistribution and use in source and binary forms, with or without
                            modification, are permitted provided that the following conditions are met:&lt;/p&gt;
                            &lt;ol&gt;
                            &lt;li&gt;Redistributions of source code must retain the above copyright notice, this
                            list of conditions and the following disclaimer.&lt;/li&gt;
                            &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice,
                            this list of conditions and the following disclaimer in the documentation
                            and/or other materials provided with the distribution.&lt;/li&gt;
                            &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors
                            may be used to endorse or promote products derived from this software
                            without specific prior written permission.&lt;/li&gt;
                            &lt;/ol&gt;
                            &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &amp;quot;AS IS&amp;quot; AND
                            ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                            WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
                            DISCLAIMED. IN NO EVENT SHALL OPEN MOBILE PLATFORM LLC OR CONTRIBUTORS BE
                            LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
                            CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
                            GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
                            HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
                            LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
                            OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>BatteryInfoPage</name>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="24"/>
        <source>Battery</source>
        <translation>Батарея</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="30"/>
        <source>Battery info is not available</source>
        <translation>Информация о батарее недоступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="41"/>
        <source>Level</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="42"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="47"/>
        <source>Level status</source>
        <translation>Статус уровня</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="53"/>
        <source>Voltage</source>
        <translation>Напряжение</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="55"/>
        <source>%L1 mV</source>
        <translation>%L1 мВ</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="56"/>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="71"/>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="80"/>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="88"/>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="95"/>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="122"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="62"/>
        <source>Health</source>
        <translation>Здоровье</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="68"/>
        <source>Maximum capacity</source>
        <translation>Максимальная емкость</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="70"/>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="79"/>
        <source>%L1 mAh</source>
        <translation>%L1 мАч</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="77"/>
        <source>Remaining capacity</source>
        <translation>Оставшаяся емкость</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="85"/>
        <source>Cycle count</source>
        <translation>Число циклов</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="93"/>
        <source>Temperature</source>
        <translation>Температура</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="96"/>
        <source>%L1 °C</source>
        <translation>%L1 °C</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="101"/>
        <source>Charging state</source>
        <translation>Состояние зарядки</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="107"/>
        <source>Charger type</source>
        <translation>Тип зарядки</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="113"/>
        <source>Current flow</source>
        <translation>Ток</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="114"/>
        <source>%L1 mA</source>
        <translation>%L1 мА</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="119"/>
        <source>Remaining charging time</source>
        <translation>Оставшееся время зарядки</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/BatteryInfoPage.qml" line="121"/>
        <source>%L1 sec</source>
        <translation>%L1 сек</translation>
    </message>
</context>
<context>
    <name>BatteryInfoUiStrings</name>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="13"/>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="27"/>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="41"/>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="54"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="16"/>
        <source>Wall charger</source>
        <translation>Зарядное устройство</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="17"/>
        <source>USB charger</source>
        <translation>USB зарядка</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="18"/>
        <source>Variable current charger</source>
        <translation>Зарядка переменного тока</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="30"/>
        <source>Charging</source>
        <translation>Заряжается</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="31"/>
        <source>Idle</source>
        <translation>Простаивает</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="32"/>
        <source>Discharging</source>
        <translation>Разряжается</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="44"/>
        <source>Bad</source>
        <translation>Плохое</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="45"/>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="59"/>
        <source>Ok</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="57"/>
        <source>Empty</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="58"/>
        <source>Low</source>
        <translation>Низкий</translation>
    </message>
    <message>
        <location filename="../qml/js/BatteryInfoUiStrings.js" line="60"/>
        <source>Full</source>
        <translation>Полный</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="11"/>
        <source>System Info</source>
        <translation>Информация о системе</translation>
    </message>
</context>
<context>
    <name>DeviceInfoPage</name>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="27"/>
        <source>Product name</source>
        <translation>Имя продукта</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="33"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="39"/>
        <source>Board name</source>
        <translation>Имя платы</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="45"/>
        <source>Manufacturer</source>
        <translation>Производитель</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="51"/>
        <source>Operating system</source>
        <translation>Операционная система</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="57"/>
        <source>OS version</source>
        <translation>Версия ОС</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="63"/>
        <source>Firmware version</source>
        <translation>Версия прошивки</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="69"/>
        <source>Unique device ID</source>
        <translation>Уникальный ИД устройства</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="75"/>
        <source>IUDID</source>
        <translation>IUDID</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="83"/>
        <source>Serial Number</source>
        <translation>Серийный номер</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="98"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="108"/>
        <source>Thermal state</source>
        <translation>Тепловое состояние</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="114"/>
        <source>Bluetooth available</source>
        <translation>Bluetooth доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="120"/>
        <source>Bluetooth enabled</source>
        <translation>Bluetooth включен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="126"/>
        <source>Camera available</source>
        <translation>Камера доступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="132"/>
        <source>FM radio available</source>
        <translation>FM радио доступно</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="138"/>
        <source>FM transmitter available</source>
        <translation>FM передатчик доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="144"/>
        <source>Infrared available</source>
        <translation>Инфракрасный порт доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="150"/>
        <source>LED available</source>
        <translation>LED доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="156"/>
        <source>Memory card slot available</source>
        <translation>Слот для карты памяти доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="162"/>
        <source>USB available</source>
        <translation>USB доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="168"/>
        <source>Vibration available</source>
        <translation>Вибрация доступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="174"/>
        <source>WLAN available</source>
        <translation>WLAN доступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="180"/>
        <source>SIM slot available</source>
        <translation>Слот SIM доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="186"/>
        <source>Positioning available</source>
        <translation>Позиционирования доступно</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="192"/>
        <source>Video output available</source>
        <translation>Видео выход доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="198"/>
        <source>Haptics available</source>
        <translation>Обратная связь доступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/DeviceInfoPage.qml" line="204"/>
        <source>NFC available</source>
        <translation>NFC доступно</translation>
    </message>
</context>
<context>
    <name>DeviceInfoUiStrings</name>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="18"/>
        <source>No lock</source>
        <translation>Нет блокировки</translation>
    </message>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="19"/>
        <source>PIN lock</source>
        <translation>Блокировка PIN</translation>
    </message>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="20"/>
        <source>Touch or keyboard lock</source>
        <translation>Сенсорная блокировка или блокировка клавиатуры</translation>
    </message>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="29"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="32"/>
        <source>Normal</source>
        <translation>Нормальное</translation>
    </message>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="33"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="34"/>
        <source>Alert</source>
        <translation>Тревога</translation>
    </message>
    <message>
        <location filename="../qml/js/DeviceInfoUiStrings.js" line="35"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>FeaturesPage</name>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="21"/>
        <source>Features are not available</source>
        <translation>Функции недоступны</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="32"/>
        <source>OS</source>
        <translation>ОС</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="33"/>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="39"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="38"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="44"/>
        <source>RAM total size</source>
        <translation>Общий объем ОЗУ</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="50"/>
        <source>RAM used size</source>
        <translation>Используемый объем ОЗУ</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="56"/>
        <source>RAM free size</source>
        <translation>Свободный объем ОЗУ</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="62"/>
        <source>CPU clock speed</source>
        <translation>Тактовая частота процессора</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="63"/>
        <source>%1 Hz</source>
        <translation>%1 Гц</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="68"/>
        <source>CPU cores number</source>
        <translation>Количество ядер процессора</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="74"/>
        <source>Battery charge</source>
        <translation>Заряд батареи</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="75"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="80"/>
        <source>Screen resolution</source>
        <translation>Разрешение экрана</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="81"/>
        <source>%1x%2</source>
        <translation>%1x%2</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="86"/>
        <source>Back camera res.</source>
        <translation>Разреш. задней камеры.</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="87"/>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="93"/>
        <source>%1 MP</source>
        <translation>%1 МП</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="92"/>
        <source>Front camera res.</source>
        <translation>Разреш. передней камеры.</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="98"/>
        <source>Has Bluetooth</source>
        <translation>Есть Bluetooth</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="104"/>
        <source>Has GNSS</source>
        <translation>Есть GNSS</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="110"/>
        <source>Has WLAN</source>
        <translation>Есть WLAN</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/FeaturesPage.qml" line="116"/>
        <source>Has NFC</source>
        <translation>Есть NFC</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="9"/>
        <source>System Info</source>
        <translation>Информация о системе</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="35"/>
        <source>Device Info</source>
        <translation>Информация об устройстве</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="36"/>
        <source>Properties of the device and the OS</source>
        <translation>Свойства устройства и ОС</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="38"/>
        <location filename="../qml/pages/MainPage.qml" line="45"/>
        <location filename="../qml/pages/MainPage.qml" line="52"/>
        <source>Hardware</source>
        <translation>Аппаратное обеспечение</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="42"/>
        <source>Battery</source>
        <translation>Батарея</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="43"/>
        <source>Properties and status of the battery</source>
        <translation>Свойства и статус батареи</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="49"/>
        <source>Storage</source>
        <translation>Хранилище</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="50"/>
        <source>Properties of the mounted volumes</source>
        <translation>Свойства подключенных томов</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="56"/>
        <source>Network Status</source>
        <translation>Статус сети</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="57"/>
        <source>Network connection status</source>
        <translation>Статус подключения к сети</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="59"/>
        <location filename="../qml/pages/MainPage.qml" line="66"/>
        <source>Connections</source>
        <translation>Соединения</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>WLAN</source>
        <translation>WLAN</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="64"/>
        <source>Properties of the WLAN module</source>
        <translation>Свойства модуля WLAN</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="70"/>
        <source>Features</source>
        <translation>Функции</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="71"/>
        <source>Features info</source>
        <translation>Информация о функциях</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="73"/>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <location filename="../qml/pages/MainPage.qml" line="87"/>
        <source>Device Info API</source>
        <translation>API информации об устройстве</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="77"/>
        <source>Sim Cards</source>
        <translation>Сим-карты</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="78"/>
        <source>SIM cards info</source>
        <translation>Информация о SIM-картах</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="84"/>
        <source>Storages</source>
        <translation>Хранилища</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="85"/>
        <source>Storages info</source>
        <translation>Информация о хранилищах</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="91"/>
        <source>Search packages</source>
        <translation>Поиск пакетов</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="92"/>
        <source>Properties of the device packages</source>
        <translation>Свойства пакетов устройства</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="94"/>
        <source>Packages</source>
        <translation>Пакеты</translation>
    </message>
</context>
<context>
    <name>NetworkManagerPage</name>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="21"/>
        <source>Network manager is not available</source>
        <translation>Сетевой менеджер недоступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="32"/>
        <source>Available</source>
        <translation>Доступен</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="38"/>
        <source>State</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="44"/>
        <source>Connected</source>
        <translation>Подключено</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="50"/>
        <source>Connecting</source>
        <translation>Подключение</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="56"/>
        <source>Connecting Wi-Fi</source>
        <translation>Подключение к Wi-Fi</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="62"/>
        <source>Offline mode</source>
        <translation>Режим оффлайн</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="68"/>
        <source>Session mode</source>
        <translation>Режим сессии</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="74"/>
        <source>Input request timeout</source>
        <translation>Таймаут запроса ввода</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="80"/>
        <source>Powered technologies</source>
        <translation>Запитанные технологии</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="81"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="86"/>
        <source>Default route</source>
        <translation>Маршрут по умолчанию</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/NetworkManagerPage.qml" line="108"/>
        <source>Connected Wi-Fi</source>
        <translation>Подключено к Wi-Fi</translation>
    </message>
</context>
<context>
    <name>NetworkServiceItem</name>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="22"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="226"/>
        <source>Hidden</source>
        <translation>Скрыто</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="27"/>
        <source>Network service is not available</source>
        <translation>Сетевая служба недоступна</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="52"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="53"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="251"/>
        <source>Undefined</source>
        <translation>Неопределенный</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="59"/>
        <source>State</source>
        <translation>Состояния</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="65"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="66"/>
        <source>No error</source>
        <translation>Нет ошибки</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="72"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="78"/>
        <source>Strength</source>
        <translation>Сила сигнала</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="85"/>
        <source>Favorite</source>
        <translation>Избранное</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="91"/>
        <source>Auto connect</source>
        <translation>Авто-подключение</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="97"/>
        <source>Connected</source>
        <translation>Подключено</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="133"/>
        <source>Nameservers</source>
        <translation>Серверы имен</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="134"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="141"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="147"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="154"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="160"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="167"/>
        <location filename="../qml/components/NetworkServiceItem.qml" line="239"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="140"/>
        <source>Nameservers config</source>
        <translation>Конфигурация серверов имен</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="146"/>
        <source>Timeservers</source>
        <translation>Серверы времени</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="153"/>
        <source>Timeservers config</source>
        <translation>Конфигурация сервереов имен</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="159"/>
        <source>Domains</source>
        <translation>Домены</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="166"/>
        <source>Domains config</source>
        <translation>Конфигурация доменов</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="172"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="178"/>
        <source>IPv4 Config</source>
        <translation>Конфигурация IPv4</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="184"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="190"/>
        <source>IPv6 Config</source>
        <translation>Конфигурация IPv6</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="205"/>
        <source>Roaming</source>
        <translation>Роуминг</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="220"/>
        <source>BSSID</source>
        <translation>BSSID</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="232"/>
        <source>Security</source>
        <translation>Защита</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="238"/>
        <source>Security type</source>
        <translation>Тип защиты</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="244"/>
        <source>Private key</source>
        <translation>Закрытый ключ</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="250"/>
        <source>Max rate</source>
        <translation>Максимальная скорость</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="256"/>
        <source>Frequency</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="262"/>
        <source>Encryption mode</source>
        <translation>Тип шифрования</translation>
    </message>
    <message>
        <location filename="../qml/components/NetworkServiceItem.qml" line="268"/>
        <source>Private key passphrase</source>
        <translation>Парольная фраза закрытого ключа</translation>
    </message>
</context>
<context>
    <name>NetworkUiStrings</name>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="13"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="29"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="66"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="93"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="94"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="119"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="135"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="150"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="168"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="16"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="122"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="138"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="17"/>
        <source>Mixed</source>
        <translation>Смешанный</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="32"/>
        <source>Out of range</source>
        <translation>Вне диапазона</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="33"/>
        <source>PIN missing</source>
        <translation>Отсутствует PIN</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="34"/>
        <source>DHCP failed</source>
        <translation>Ошибка DHCP</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="35"/>
        <source>Connect failed</source>
        <translation>Ошибка подключения</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="36"/>
        <source>Login failed</source>
        <translation>Ошибка авторизации</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="37"/>
        <source>Auth failed</source>
        <translation>Ошибка аутентификации</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="38"/>
        <source>Invalid key</source>
        <translation>Невереный ключ</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="47"/>
        <source>%L1 GHz</source>
        <translation>%L1 ГГц</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="58"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="84"/>
        <source>Undefined</source>
        <translation>Неопределенный</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="61"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="87"/>
        <source>Address: %1</source>
        <translation>Адрес: %1</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="63"/>
        <source>Netmask: %1</source>
        <translation>Сетевая маска: %1</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="65"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="91"/>
        <source>Gateway: %1</source>
        <translation>Шлюз: %1</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="66"/>
        <location filename="../qml/js/NetworkUiStrings.js" line="94"/>
        <source>Method: %1</source>
        <translation>Метод: %1</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="71"/>
        <source>Manual</source>
        <translation>Ручной</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="72"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="73"/>
        <source>Off</source>
        <translation>Выключено</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="74"/>
        <source>Fixed</source>
        <translation>Фиксированный</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="89"/>
        <source>Prefix length: %1</source>
        <translation>Длина префикса: %1</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="93"/>
        <source>Privacy: %1</source>
        <translation>Приватность: %1</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="98"/>
        <source>Disabled</source>
        <translation>Выключено</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="99"/>
        <source>Enabled</source>
        <translation>Включено</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="100"/>
        <source>Prefered</source>
        <translation>Предпочтительно</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="109"/>
        <source>%L1 Mb/s</source>
        <translation>%L1 Мб/сек</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="153"/>
        <source>Idle</source>
        <translation>Простаивает</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="154"/>
        <source>Failure</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="155"/>
        <source>Association</source>
        <translation>Ассоциация</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="156"/>
        <source>Configuration</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="157"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="158"/>
        <source>Disconnect</source>
        <translation>Отключено</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="159"/>
        <source>Online</source>
        <translation>Онлайн</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="171"/>
        <source>Cellular</source>
        <translation>Сотовая сеть</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="172"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/js/NetworkUiStrings.js" line="173"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PackageInfoPage</name>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="48"/>
        <source>Package information</source>
        <translation>Информация о пакете</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="86"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="96"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="106"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="116"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="126"/>
        <source>Architecture</source>
        <translation>Архитектура</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="136"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="146"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="156"/>
        <source>Summary</source>
        <translation>Сводка</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackageInfoPage.qml" line="167"/>
        <source>Description</source>
        <translation>Описания</translation>
    </message>
</context>
<context>
    <name>PackageInfoProvider</name>
    <message>
        <location filename="../src/packageinfoprovider.cpp" line="98"/>
        <source>Transaction failed</source>
        <translation>Ошибка транзакции</translation>
    </message>
    <message>
        <location filename="../src/packageinfoprovider.cpp" line="101"/>
        <source>Transaction faild unexpectidly</source>
        <translation>Неожиданная ошибка транзакции</translation>
    </message>
    <message>
        <location filename="../src/packageinfoprovider.cpp" line="124"/>
        <source>Installed</source>
        <translation>Установлено</translation>
    </message>
    <message>
        <location filename="../src/packageinfoprovider.cpp" line="127"/>
        <source>Available</source>
        <translation>Доступен</translation>
    </message>
    <message>
        <location filename="../src/packageinfoprovider.cpp" line="130"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
</context>
<context>
    <name>PackageSearcher</name>
    <message>
        <location filename="../src/packagesearcher.cpp" line="112"/>
        <source>Installed</source>
        <translation>Установлено</translation>
    </message>
    <message>
        <location filename="../src/packagesearcher.cpp" line="115"/>
        <source>Available</source>
        <translation>Доступен</translation>
    </message>
    <message>
        <location filename="../src/packagesearcher.cpp" line="118"/>
        <source>Unknown</source>
        <translation>Неизвестно</translation>
    </message>
</context>
<context>
    <name>PackagesSearchPage</name>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="70"/>
        <source>package name</source>
        <translation>имя пакета</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="96"/>
        <source>Searching for %1</source>
        <translation>Поиск %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="105"/>
        <source>Nothing was found for %1</source>
        <translation>Ничего не найдено для %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="114"/>
        <source>Search results for %1</source>
        <translation>Результаты поиска для %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="162"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="172"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="182"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="192"/>
        <source>Architecture</source>
        <translation>Архитектура</translation>
    </message>
    <message>
        <location filename="../qml/pages/packages/PackagesSearchPage.qml" line="202"/>
        <source>Summary</source>
        <translation>Сводка</translation>
    </message>
</context>
<context>
    <name>SimCardsPage</name>
    <message>
        <location filename="../qml/pages/deviceinfo/SimCardsPage.qml" line="23"/>
        <source>SIM card</source>
        <translation>SIM карта</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/SimCardsPage.qml" line="42"/>
        <source>SIM cards info is not available</source>
        <translation>Информация о SIM-картах недоступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/SimCardsPage.qml" line="53"/>
        <source>SIM card name</source>
        <translation>SIM card name</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/SimCardsPage.qml" line="59"/>
        <source>Operator name</source>
        <translation>Имя оператора</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/SimCardsPage.qml" line="65"/>
        <source>SIM card enabled</source>
        <translation>SIM-карта включена</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/SimCardsPage.qml" line="71"/>
        <source>Preferred voice call</source>
        <translation>Предпочтительный голосовой вызов</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/SimCardsPage.qml" line="77"/>
        <source>Preferred data transfer</source>
        <translation>Предпочтительная передача данных</translation>
    </message>
</context>
<context>
    <name>StorageInfoPage</name>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="35"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="41"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="47"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="53"/>
        <source>File system type</source>
        <translation>Тип файловой системы</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="59"/>
        <source>Block size</source>
        <translation>Размер блока</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="65"/>
        <source>Available size</source>
        <translation>Доступный размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="71"/>
        <source>Free size</source>
        <translation>Свободный размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="77"/>
        <source>Total size</source>
        <translation>Общий размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="83"/>
        <source>Read only</source>
        <translation>Только для чтения</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="89"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="../qml/pages/hardware/StorageInfoPage.qml" line="95"/>
        <source>Is root</source>
        <translation>Корневой</translation>
    </message>
</context>
<context>
    <name>StorageInfoUiStrings</name>
    <message>
        <location filename="../qml/js/StorageInfoUiStrings.js" line="11"/>
        <source>%L1 MB</source>
        <translation>%L1 МБ</translation>
    </message>
</context>
<context>
    <name>StoragesPage</name>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="21"/>
        <source>Storages info is not available</source>
        <translation>Информация о хранилищах недоступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="36"/>
        <source>Internal storage</source>
        <translation>Внутренняя память</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="41"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="81"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="122"/>
        <source>Device label</source>
        <translation>Метка устройства</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="47"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="87"/>
        <source>File system type</source>
        <translation>Тип файловой системы</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="53"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="93"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="134"/>
        <source>Total size</source>
        <translation>Общий размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="59"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="99"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="140"/>
        <source>Used size</source>
        <translation>Используемый размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="65"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="105"/>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="146"/>
        <source>Free size</source>
        <translation>Свободный размер</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="76"/>
        <source>Internal user partition</source>
        <translation>Внутренний пользовательский раздел</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="117"/>
        <source>External storage</source>
        <translation>Внешнее хранилище</translation>
    </message>
    <message>
        <location filename="../qml/pages/deviceinfo/StoragesPage.qml" line="128"/>
        <source>Partitions count</source>
        <translation>Количество разделов</translation>
    </message>
</context>
<context>
    <name>UiStrings</name>
    <message>
        <location filename="../qml/js/UiStrings.js" line="11"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../qml/js/UiStrings.js" line="11"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>WlanInfoPage</name>
    <message>
        <location filename="../qml/pages/connections/WlanInfoPage.qml" line="28"/>
        <source>Update</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/WlanInfoPage.qml" line="37"/>
        <source>The WLAN is not available</source>
        <translation>WLAN недоступна</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/WlanInfoPage.qml" line="38"/>
        <source>The WLAN is not powered</source>
        <translation>WLAN не запитана</translation>
    </message>
    <message>
        <location filename="../qml/pages/connections/WlanInfoPage.qml" line="39"/>
        <source>Turn the WLAN on</source>
        <translation>Включить WLAN</translation>
    </message>
</context>
</TS>
